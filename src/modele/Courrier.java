/*__________________________________________________________*/
/*__________________________________________________________*/
/**
 * Fichier : Courier.java
 * 
 * créé le : 5 févr. 2014 à 16:07:15
 *
 * Auteur : Grégoire Chamberland
 */
/*__________________________________________________________*/
package modele;

import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.SimpleTimeZone;
import java.util.TimeZone;
import java.util.TreeSet;

import org.apache.log4j.lf5.util.DateFormatManager;

import modele.mime.ContenuMIME;

/*__________________________________________________________*/
/**
 */
/*__________________________________________________________*/
public class Courrier
{	
	private TreeSet<String> tousDestinataires = new TreeSet<String>();
	
	private String adresseExpediteur;
	private String nomExpediteur;
	
	private String entete;
	
	private ContenuMIME contenu;
	
	public Courrier(String adresseExpediteur,
			String[] destinataires, 
			String[] destinatairesCopiesCarbone,
			String[] destinatairesCopiesCarboneInvisibles,
			String sujet,
			ContenuMIME contenu)
	{
		this(adresseExpediteur, null, destinataires, destinatairesCopiesCarbone, destinatairesCopiesCarboneInvisibles, sujet, contenu);
	}
	
	public Courrier(String adresseExpediteur,
			String nomExpediteur,
			String[] destinataires, 
			String[] destinatairesCopiesCarbone,
			String[] destinatairesCopiesCarboneInvisibles,
			String sujet,
			ContenuMIME contenu)
	{	
		List<String> listeDestinataires = convertirTableauEnListe(destinataires);
		List<String> listeDestinatairesCopiesCarbone = convertirTableauEnListe(destinatairesCopiesCarbone);
		List<String> listeDestinatairesCopiesCarboneInvisibles = convertirTableauEnListe(destinatairesCopiesCarboneInvisibles);
		
		tousDestinataires.addAll(listeDestinataires);
		tousDestinataires.addAll(listeDestinatairesCopiesCarbone);
		tousDestinataires.addAll(listeDestinatairesCopiesCarboneInvisibles);
		
		this.adresseExpediteur = adresseExpediteur;
		this.contenu = contenu;
		
		this.nomExpediteur = nomExpediteur;
		
		entete = creerEntete(adresseExpediteur, listeDestinataires, listeDestinatairesCopiesCarbone, sujet);
	}

	/*__________________________________________________________*/
	/**
	 * @return
	 */
	public String getExpediteur()
	{
		return adresseExpediteur;
	}

	/*__________________________________________________________*/
	/**
	 * @return
	 */
	public ArrayList<String> getTousDestinataires()
	{
		return new ArrayList<String>(tousDestinataires);
	}
	
	private String infosExpediteur()
	{
		if (nomExpediteur != null)
			return String.format("%s <%s>", ContenuMIME.getBase64UTF8EncodedWordSyntaxe(nomExpediteur), adresseExpediteur);
		else
			return String.format("<%s>", adresseExpediteur);
	}

	private String creerEntete(String expediteur,
			List<String> listeDestinataires,
			List<String> listeDestinatairesCopiesCarbone,
			String sujet)
	{
		StringBuffer entete = new StringBuffer(String.format("Message-ID: <%s>\r\n", ContenuMIME.genContentId()));
		
		entete.append(String.format("From: %s\r\n", infosExpediteur()));
		
		ecrireChampEnveloppe(entete, "To", listeDestinataires);
		ecrireChampEnveloppe(entete, "Cc", listeDestinatairesCopiesCarbone);
		
		entete.append(String.format("Subject: %s\r\n", ContenuMIME.getBase64UTF8EncodedWordSyntaxe(sujet)));
		entete.append(String.format("Date: %s\r\n", getGMTDate()));
		entete.append("X-Mailer: Buggy 1.0\r\n");
		
		System.out.println(entete.toString());
		
		return entete.toString();
	}
	
	private void ecrireChampEnveloppe(StringBuffer entete, String nomChamp, List<String> listeAdresses)
	{
		if (listeAdresses.size() == 0)
			return;
		
		entete.append(String.format("%s: %s", nomChamp, listeAdresses.get(0)));
		
		if (listeAdresses.size() > 1)
			for (int i=1; i<listeAdresses.size(); i++)
				entete.append(String.format(", %s", listeAdresses.get(i)));
		
		entete.append("\r\n");
	}
	
	private ArrayList<String> convertirTableauEnListe(String[] tab)
	{
		ArrayList<String> liste = new ArrayList<String>();
		
		for (String s : tab)
		{
			liste.add(s);
		}
		
		return liste;
	}
	
	public String getGMTDate()
	{
		SimpleDateFormat dateFormatGmt = new SimpleDateFormat();
		dateFormatGmt.setTimeZone(new SimpleTimeZone(SimpleTimeZone.UTC_TIME, "UTC"));
		
		return dateFormatGmt.format(new Date());
	}
	
	public long getTaille() throws Exception
	{
		return entete.length() + contenu.getTaille();
	}
	
	public void ecrire(BufferedWriter out) throws Exception
	{
		out.write(entete);
		contenu.ecrire(out);
	}
}

/*__________________________________________________________*/
/*   Fin du fichier Courier.java
/*__________________________________________________________*/