package modele.mime;

import java.io.BufferedWriter;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
/*__________________________________________________________*/
/*__________________________________________________________*/
/**
 * Fichier : ContenuMIME.java
 * 
 * créé le : 5 févr. 2014 à 20:57:51
 *
 * Auteur : Grégoire Chamberland
 */
/*__________________________________________________________*/

/*__________________________________________________________*/
/**
 */
/*__________________________________________________________*/
public abstract class ContenuMIME
{
	private static String hostName = null;
	
	public static enum ContentDisposition
	{
		Attachement("attachement"),
		Inline("inline");
		
		private String dispositionKeyWord;
		
		ContentDisposition(String dispositionKeyWord)
		{
			this.dispositionKeyWord = dispositionKeyWord;
		}

		/*__________________________________________________________*/
		/**
		 * @return
		 * @see java.lang.Enum#toString()
		 */
		@Override
		public String toString()
		{
			return dispositionKeyWord;
		}
	}
	
	public static enum ContentEncoding
	{
		Binary("binary"),
		Base64("base64"),
		MIME8Bit("8bit");
		
		private String encoding;

		ContentEncoding(String encoding)
		{
			this.encoding = encoding;
		}
		
		/*__________________________________________________________*/
		/**
		 * @return
		 * @see java.lang.Enum#toString()
		 */
		@Override
		public String toString()
		{
			return encoding;
		}
	}
	
	private String contentType;
	
	private String versionMIME;
	
	private List<String> parametresEntete = new ArrayList<String>();
	
	public void setVersionMIME(String version)
	{
		this.versionMIME = version;
		if (parametresEntete.size() > 2)
			parametresEntete.remove(1);
		
		parametresEntete.add(1, String.format("MIME-version: %s\r\n", this.versionMIME));
	}
	
	public String getVersionMIME()
	{
		return versionMIME;
	}
	
	public void setContentType(String contentType)
	{
		this.contentType = contentType;
		
		parametresEntete.remove(0);
		
		parametresEntete.add(0, String.format("Content-type: %s\r\n", this.contentType));
	}
	
	public String getContentType()
	{
		return contentType;
	}
	
	public ContenuMIME(String contentType)
	{
		this(contentType, "1.0");
	}
	
	public ContenuMIME(String contentType, String versionMIME)
	{
		parametresEntete.add("");
		parametresEntete.add("");
		parametresEntete.add("\r\n");
		
		setContentType(contentType);
		setVersionMIME(versionMIME);
	}
	
	protected void ajouterParametreEntete(String parametre)
	{
		parametresEntete.add(parametresEntete.size()-1, String.format("%s\r\n", parametre));
	}
	
	private final void ecrireEntete(BufferedWriter out) throws Exception
	{	
		for (String parametre : parametresEntete)
		{
			out.write(parametre);
		}
	}
	
	public void ecrire(BufferedWriter out) throws Exception
	{
		ecrireEntete(out);
		ecrireContenu(out);
		out.write("\r\n");
	}
	
	private long getTailleEntete()
	{
		long tailleEntete = 0;
		
		for (String param : parametresEntete)
			tailleEntete += param.length();
		
		return tailleEntete;
	}
	
	public final long getTaille() throws Exception
	{
		return getTailleEntete() + getTailleContenu() + 2;
	}
	
	protected abstract void ecrireContenu(BufferedWriter out) throws Exception;
	public abstract long getTailleContenu() throws Exception;
	
	public static String echapperLesSymbolesDeFinDeTransmission(String texte)
	{
		texte = texte.replaceAll("^\\.", "..");
		
		texte = texte.replaceAll("\n\\.", "\n..");
		
		texte = texte.replaceAll("\r\n\\.", "\r\n..");
		
		return texte;
	}

	public static String getBase64UTF8EncodedWordSyntaxe(String valeur)
	{
		return String.format("=?UTF-8?B?%s?=", Base64Coder.encodeString(valeur));
	}
	
	public static String genContentId()
	{
		String uuid = UUID.randomUUID().toString();
		
		try
		{	
			if (hostName == null)
				hostName = InetAddress.getLocalHost().getHostName();
			return String.format("%s@%s", uuid, hostName);
		} catch (UnknownHostException e)
		{
			return String.format("%s@unknown", uuid);
		}
	}
}

/*__________________________________________________________*/
/*   Fin du fichier ContenuMIME.java
/*__________________________________________________________*/