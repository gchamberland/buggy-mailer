/*__________________________________________________________*/
/*__________________________________________________________*/
/**
 * Fichier : MessageMIMESimple.java
 * 
 * créé le : 6 févr. 2014 à 00:56:46
 *
 * Auteur : Grégoire Chamberland
 */
/*__________________________________________________________*/
package modele.mime;

import java.io.BufferedWriter;
import java.io.OutputStream;

import modele.mime.ContenuMIME.ContentEncoding;

/*__________________________________________________________*/
/**
 */
/*__________________________________________________________*/
public class MessageMIMESimple extends ContenuMIME
{
	private String texte;
	private ContentEncoding encoding;
	
	/*__________________________________________________________*/
	/**
	 * @param contentType
	 * @throws Exception 
	 */
	public MessageMIMESimple(String texte, ContentEncoding encoding)
	{
		super("text/plain; charset=UTF-8");
		
		this.texte = texte;
		this.encoding = encoding;
		
		ajouterParametreEntete(String.format("Content-Transfer-Encoding: %s", encoding));
	}

	/*__________________________________________________________*/
	/**
	 * @param out
	 * @throws Exception
	 * @see modele.mime.ContenuMIME#ecrireContenu(java.io.OutputStream)
	 */
	@Override
	protected void ecrireContenu(BufferedWriter out) throws Exception
	{
		switch (encoding)
		{
			case Base64:
				out.write(String.format("%s\r\n", Base64Coder.encodeString(texte)));
			break;
			
			case MIME8Bit:
				out.write(ContenuMIME.echapperLesSymbolesDeFinDeTransmission(texte));
			break;
			
			default:
				throw new Exception("Mode d'encodage incompatible");
		}
	}
	
	/*__________________________________________________________*/
	/**
	 * @return
	 * @see modele.mime.ContenuMIME#getTailleContenu()
	 */
	@Override
	public long getTailleContenu()
	{
		long tailleTexte = texte.length();
		
		if (encoding == ContentEncoding.MIME8Bit)
			return tailleTexte;
		else if (encoding == ContentEncoding.Base64)
		{
			return ((tailleTexte + ( (tailleTexte % 3) != 0 ? (3 - (tailleTexte % 3)) : 0) ) / 3) * 4 + 2;
		}
		else 
		{
			return -1;
		}
	}

}

/*__________________________________________________________*/
/*   Fin du fichier MessageMIMESimple.java
/*__________________________________________________________*/