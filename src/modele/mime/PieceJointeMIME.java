/*__________________________________________________________*/
/*__________________________________________________________*/
/**
 * Fichier : PieceJointeMIME.java
 * 
 * créé le : 5 févr. 2014 à 22:29:40
 *
 * Auteur : Grégoire Chamberland
 */
/*__________________________________________________________*/
package modele.mime;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.Socket;

import javax.net.ssl.SSLSocketFactory;

import net.sf.jmimemagic.Magic;
import net.sf.jmimemagic.MagicException;
import net.sf.jmimemagic.MagicMatch;
import net.sf.jmimemagic.MagicMatchNotFoundException;
import net.sf.jmimemagic.MagicParseException;

/*__________________________________________________________*/
/**
 */
/*__________________________________________________________*/
public class PieceJointeMIME extends ContenuMIME
{	
	private File file;
	private ContentDisposition disposition;
	private ContentEncoding encoding;
	
	/*__________________________________________________________*/
	/**
	 * @param contentType
	 * @throws MagicException 
	 * @throws MagicMatchNotFoundException 
	 * @throws MagicParseException 
	 */
	public PieceJointeMIME(File file, ContentDisposition disposition, ContentEncoding encoding) throws Exception
	{
		super("");
		
		this.file = file;
		this.disposition = disposition;
		this.encoding = encoding;
		
		try
		{
			MagicMatch match = Magic.getMagicMatch(file, true);
			setContentType(String.format("%s; name=\"%s\"", match.getMimeType(), getBase64UTF8EncodedWordSyntaxe(file.getName())));
		}
		catch (MagicMatchNotFoundException e)
		{
			setContentType(String.format("application/octet-stream; name=\"%s\"", getBase64UTF8EncodedWordSyntaxe(file.getName())));
		}
		
		
		
		ajouterParametreEntete(String.format("Content-disposition: %s", disposition));
		ajouterParametreEntete(String.format("Content-Transfer-Encoding: %s", encoding));
	}

	/*__________________________________________________________*/
	/**
	 * @param out
	 * @throws Exception
	 * @see modele.mime.ContenuMIME#ecrireContenu(java.io.OutputStream)
	 */
	@Override
	protected void ecrireContenu(BufferedWriter out) throws Exception
	{
		switch (encoding)
		{
			case Base64:
				ecrireContenuEnBase64(out);
			break;
			
			case Binary:
				ecrireContenuEnBinaire(out);
			break;
			
			case MIME8Bit:
				ecrireContenuEn8Bit(out);
			break;
				
			default:
				throw new Exception("Mode d'encodage incompatible");
		}
	}

	/*__________________________________________________________*/
	/**
	 * @param out
	 * @throws Exception 
	 */
	private void ecrireContenuEn8Bit(BufferedWriter out) throws Exception
	{
		int n;
		char[] buffer = new char[999];
		BufferedReader fileInput = new BufferedReader(new InputStreamReader(new FileInputStream(file)));
		
		buffer[997] = '\r';
		buffer[998] = '\n';
		
		do
		{
			n = fileInput.read(buffer, 0, 997);
			
			if (n != -1)
			{
				String buf = new String(buffer, 0, n < 997 ? n : 999);
				
				out.write(echapperLesSymbolesDeFinDeTransmission(buf));
			}
		}
		while (n>0);
		
		fileInput.close();
	}

	/*__________________________________________________________*/
	/**
	 * @param out
	 * @throws Exception 
	 */
	private void ecrireContenuEnBinaire(BufferedWriter out) throws Exception
	{
		byte[] buffer = new byte[10240];
		int n;
		
		BufferedInputStream fileInput = new BufferedInputStream(new FileInputStream(file));
		
		do
		{
			n = fileInput.read(buffer, 0, 10240);
			
			out.write(new String(buffer, 0, n));
		}
		while (n>0);
		
		fileInput.close();
	}

	/*__________________________________________________________*/
	/**
	 * @param out
	 * @throws Exception 
	 */
	private void ecrireContenuEnBase64(BufferedWriter out) throws Exception
	{
		byte[] buffer = new byte[2736];
		int n;
		int cpt = 0;
		
		BufferedInputStream fileInput = new BufferedInputStream(new FileInputStream(file));
		
		do
		{
			n = fileInput.read(buffer, 0, 2736);
			
			out.write(Base64Coder.encodeLines(buffer, 0, n, 76, "\r\n"));
		}
		while (n>0);
		
		fileInput.close();
	}

	/*__________________________________________________________*/
	/**
	 * @return
	 * @throws Exception 
	 * @see modele.mime.ContenuMIME#getTailleContenu()
	 */
	@Override
	public long getTailleContenu() throws Exception
	{
		long tailleFichier = file.length();
		
		if (encoding == ContentEncoding.Binary)
			return tailleFichier;
		else if (encoding == ContentEncoding.Base64)
		{
			long tailleB64 = ((tailleFichier + ( (tailleFichier % 3) != 0 ? (3 - (tailleFichier % 3)) : 0) ) / 3) << 2;
			
			return tailleB64 + (tailleB64 / 76 << 1);
		}
		else if (encoding == ContentEncoding.MIME8Bit)
		{
			return tailleFichier + (tailleFichier / 998 << 1);
		}
		else
		{
			throw new Exception("Mode d'encodage incompatible");
		}
	}
}

/*__________________________________________________________*/
/*   Fin du fichier PieceJointeMIME.java
/*__________________________________________________________*/