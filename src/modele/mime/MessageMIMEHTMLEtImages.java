/*__________________________________________________________*/
/*__________________________________________________________*/
/**
 * Fichier : ContenuMIMEMultipartiesReliees.java
 * 
 * créé le : 15 févr. 2014 à 03:23:57
 *
 * Auteur : Grégoire Chamberland
 */
/*__________________________________________________________*/
package modele.mime;

import java.io.File;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/*__________________________________________________________*/
/**
 */
/*__________________________________________________________*/
public class MessageMIMEHTMLEtImages extends ContenuMIMEMultiParties
{
	private String codeHTML;
	
	/*__________________________________________________________*/
	/**
	 * @param contentType
	 * @throws Exception 
	 */
	public MessageMIMEHTMLEtImages(String codeHTML, HashMap<URL, File> images,
			ContentEncoding encodageMessage, ContentEncoding encodageImages) throws Exception
	{
		super("multipart/related");
		
		this.codeHTML = codeHTML;
		
		String contentIDRacine = genContentId();
		
		this.setContentType(String.format("%s;\r\n              start=\"<%s>\"", this.getContentType(), contentIDRacine));
		
		for (URL url : images.keySet())
		{
			ajouterImage(url.toString(), images.get(url), encodageImages);
		}
		
		MessageMIMEHTML racine = new MessageMIMEHTML(this.codeHTML, encodageMessage);
		racine.ajouterParametreEntete(String.format("Content-ID: <%s>", contentIDRacine));
		
		ajouterSousPartie(0, racine);
	}
	
	private void ajouterImage(String URLSource, File f, ContentEncoding encodage)
			throws Exception
	{
		String cid = genContentId();
		
		codeHTML = remplacerURLParCID(codeHTML, URLSource, cid);
		
		try
		{
			ContenuMIME sousPartie = new PieceJointeMIME(f, ContentDisposition.Attachement, encodage);
			sousPartie.ajouterParametreEntete(String.format("Content-ID: <%s>", cid));
			this.ajouterSousPartie(sousPartie);
		} catch (Exception e)
		{}
	}
	
	private String remplacerURLParCID(String codeHTML, String URL, String CID)
	{
		Matcher match = Pattern.compile(URL).matcher(codeHTML);
		
		return match.replaceAll(String.format("cid:%s", CID));
	}
	
	
}

/*__________________________________________________________*/
/*   Fin du fichier ContenuMIMEMultipartiesReliees.java
/*__________________________________________________________*/