/*__________________________________________________________*/
/*__________________________________________________________*/
/**
 * Fichier : ContenuMIME.java
 * 
 * créé le : 5 févr. 2014 à 16:46:31
 *
 * Auteur : Grégoire Chamberland
 */
/*__________________________________________________________*/
package modele.mime;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/*__________________________________________________________*/
/**
 */
/*__________________________________________________________*/
public class ContenuMIMEMultiParties extends ContenuMIME
{
	private static final int DELIMITER_LENGHT = 25;
	
	private static Random rand = new Random();
	
	private String delimiter;
	
	private static String genererDelimiter(int nCaracteres)
	{
		char[] delimiterBytes = new char[nCaracteres-8];
		StringBuffer boundary = new StringBuffer();
		
		for (int i=0; i<delimiterBytes.length; i++)
			delimiterBytes[i] = (char)(rand.nextInt(25) + 65);
		
		boundary.append("_ _'");
		boundary.append(delimiterBytes);
		boundary.append("'_ _");
		
		return boundary.toString();
	}
	
	/*__________________________________________________________*/
	/**
	 * @param contentType
	 */
	public ContenuMIMEMultiParties(String contentType)
	{
		this(contentType, "1.0");
	}

	/*__________________________________________________________*/
	/**
	 * @param contentType
	 * @param versionMIME
	 */
	public ContenuMIMEMultiParties(String contentType, String versionMIME)
	{
		super("", versionMIME);
		
		delimiter = genererDelimiter(DELIMITER_LENGHT);
		
		setContentType(String.format("%s;\r\n              boundary=\"%s\"", contentType, delimiter));
	}

	private List<ContenuMIME> sousParties = new ArrayList<ContenuMIME>();
	
	public void ajouterSousPartie(ContenuMIME sousPartie)
	{
		this.sousParties.add(sousPartie);
	}
	
	public void ajouterSousPartie(int position, ContenuMIME sousPartie)
	{
		this.sousParties.add(position, sousPartie);
	}
	
	public void retirerSousPartie(ContenuMIME sousPartie)
	{
		this.sousParties.remove(sousPartie);
	}

	/*__________________________________________________________*/
	/**
	 * @param out
	 * @throws Exception 
	 * @see modele.mime.ContenuMIME#ecrireContenu(java.io.OutputStream)
	 */
	@Override
	protected void ecrireContenu(BufferedWriter out) throws Exception
	{
		for (ContenuMIME sousPartie : sousParties)
		{
			try
			{
				out.write(String.format("--%s\r\n", delimiter));
				sousPartie.ecrire(out);
			} 
			catch (UnsupportedEncodingException e)
			{}
		}
		
		try
		{
			out.write(String.format("--%s--", delimiter));
		} 
		catch (UnsupportedEncodingException e)
		{}
	}

	/*__________________________________________________________*/
	/**
	 * @return
	 * @throws Exception 
	 * @see modele.mime.ContenuMIME#getTailleContenu()
	 */
	@Override
	public long getTailleContenu() throws Exception
	{
		long tailleContenu = 0;
		
		for (ContenuMIME sousPartie : sousParties)
			tailleContenu += sousPartie.getTaille() + DELIMITER_LENGHT + 4;
		
		return tailleContenu + DELIMITER_LENGHT + 6;
	}
}

/*__________________________________________________________*/
/*   Fin du fichier ContenuMIME.java
/*__________________________________________________________*/