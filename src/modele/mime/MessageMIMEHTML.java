/*__________________________________________________________*/
/*__________________________________________________________*/
/**
 * Fichier : MessageMIMEHTML.java
 * 
 * créé le : 15 févr. 2014 à 05:07:22
 *
 * Auteur : Grégoire Chamberland
 */
/*__________________________________________________________*/
package modele.mime;

/*__________________________________________________________*/
/**
 */
/*__________________________________________________________*/
public class MessageMIMEHTML extends MessageMIMESimple
{

	/*__________________________________________________________*/
	/**
	 * @param texte
	 * @param encoding
	 */
	public MessageMIMEHTML(String codeHTML, ContentEncoding encoding)
	{
		super(codeHTML, encoding);
		setContentType("text/html");
	}

}

/*__________________________________________________________*/
/*   Fin du fichier MessageMIMEHTML.java
/*__________________________________________________________*/