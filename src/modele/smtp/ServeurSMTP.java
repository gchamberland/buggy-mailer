/*__________________________________________________________*/
/*__________________________________________________________*/
/**
 * Fichier : ServeurSMTP.java
 * 
 * créé le : 4 févr. 2014 à 13:49:59
 *
 * Auteur : Grégoire Chamberland
 */
/*__________________________________________________________*/
package modele.smtp;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

/*__________________________________________________________*/
/**
 */
/*__________________________________________________________*/
public class ServeurSMTP implements Serializable
{	
	/**  */
	private static final long serialVersionUID = 1083990865501656250L;

	private Set<String> mAuthTypesSupportes = new TreeSet<String>();
	
	private TypeDeSecurite typeDeSecurite;

	private boolean m8BitMimeSupport = false;

	private boolean mSizeSupport = false;

	private AuthentificationMethod authentificationMethod = null;
	
	private String nomHote;
	
	private int port;

	private String nomUtilisateur;
	
	private String adresseMailUtilisateur;
	
	public enum TypeDeSecurite 
	{ 
		NON_SECURISE("Non securisé"),
		SMTP_VIA_SSL("SMTP via SSL"),
		ESMTP_SECURISE("ESMTP sécurisé");
		
		private String type;

		TypeDeSecurite(String type)
		{
			this.type = type;
		}
		
		/*__________________________________________________________*/
		/**
		 * @return
		 * @see java.lang.Enum#toString()
		 */
		@Override
		public String toString()
		{
			return type;
		}
	};
	
	public enum TypeAuthentification
	{ 
		NON_SECURISE("Non securisé"),
		SMTP_VIA_SSL("SMTP via SSL"),
		ESMTP_SECURISE("ESMTP sécurisé");
		
		private String type;

		TypeAuthentification(String type)
		{
			this.type = type;
		}
		
		/*__________________________________________________________*/
		/**
		 * @return
		 * @see java.lang.Enum#toString()
		 */
		@Override
		public String toString()
		{
			return type;
		}
	};
	
	public ServeurSMTP(String nomHote, int port, TypeDeSecurite typeDeSecurite, String adresseMailUtilisateur, String nomUtilisateur)
	{	
		this.nomHote = nomHote;
		this.port = port;
		this.typeDeSecurite = typeDeSecurite;
		this.adresseMailUtilisateur = adresseMailUtilisateur;
		this.nomUtilisateur = nomUtilisateur;
		
		if (this.nomUtilisateur.trim().length() < 1)
			this.nomUtilisateur = null;
	}
	
	public ServeurSMTP(ServeurSMTP serveur)
	{	
		this(serveur.nomHote, serveur.port, serveur.typeDeSecurite, serveur.adresseMailUtilisateur, serveur.nomUtilisateur);
		
		this.authentificationMethod = serveur.authentificationMethod;
		this.m8BitMimeSupport = serveur.m8BitMimeSupport;
		this.mAuthTypesSupportes = new TreeSet(serveur.mAuthTypesSupportes);
		this.mSizeSupport = serveur.mSizeSupport;
	}
	
	public List<String> getTypeAuthentificationSupportees()
	{
		return new ArrayList<String>(mAuthTypesSupportes);
	}

	/*__________________________________________________________*/
	/**
	 * @param method
	 */
	public void setAuthentificationMethod(AuthentificationMethod method)
	{
		this.authentificationMethod = method;
	}

	/*__________________________________________________________*/
	/**
	 * @return
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString()
	{
		return String.format("%s:%s", nomHote, port);
	}

	/*__________________________________________________________*/
	/**
	 * @return
	 */
	public String getNomHote()
	{
		return nomHote;
	}

	/*__________________________________________________________*/
	/**
	 * @return
	 */
	public int getPort()
	{
		return port;
	}

	/*__________________________________________________________*/
	/**
	 * @return
	 */
	public TypeDeSecurite getTypeDeSecurite()
	{
		return typeDeSecurite;
	}

	/*__________________________________________________________*/
	/**
	 * @return
	 */
	public String getNomMethodeAuthentification()
	{
		return authentificationMethod.getNom();
	}

	/*__________________________________________________________*/
	/**
	 * @param s
	 */
	public void ajouterTypeAuthentification(String authTypeKey)
	{
		mAuthTypesSupportes.add(authTypeKey);
	}

	/*__________________________________________________________*/
	/**
	 * @param b
	 */
	public void setSupport8BitMIME(boolean support)
	{
		m8BitMimeSupport = support;
	}

	/*__________________________________________________________*/
	/**
	 * @param b
	 */
	public void setSupportExtensionSize(boolean support)
	{
		mSizeSupport = support;
	}

	/*__________________________________________________________*/
	/**
	 * @return
	 */
	public boolean supporteExtensionSize()
	{
		return mSizeSupport;
	}

	/*__________________________________________________________*/
	/**
	 * @return
	 */
	public AuthentificationMethod getMetodeAuthentification()
	{
		return authentificationMethod;
	}
	
	public void setMetodeAuthentification(AuthentificationMethod metode)
		throws IllegalArgumentException
	{
		if (!mAuthTypesSupportes.contains(metode.getNom()))
			throw new IllegalArgumentException("Métode d'authentification non supportée par ce serveur.");
		
		this.authentificationMethod = metode;
	}

	/*__________________________________________________________*/
	/**
	 * @return
	 */
	public String getAdresseMail()
	{
		return adresseMailUtilisateur;
	}
	
	public String getNomUtilisateur()
	{
		return nomUtilisateur;
	}

	/*__________________________________________________________*/
	/**
	 * @return
	 */
	public boolean supportExtension8BitMIME()
	{
		return m8BitMimeSupport;
	}
}

/*__________________________________________________________*/
/*   Fin du fichier ServeurSMTP.java
/*__________________________________________________________*/