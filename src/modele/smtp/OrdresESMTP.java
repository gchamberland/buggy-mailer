/*__________________________________________________________*/
/*__________________________________________________________*/
/**
 * Fichier : OrdresESMTP.java
 * 
 * créé le : 4 févr. 2014 à 14:33:01
 *
 * Auteur : Grégoire Chamberland
 */
/*__________________________________________________________*/
package modele.smtp;

/*__________________________________________________________*/
/**
 */
/*__________________________________________________________*/
public class OrdresESMTP
{
	public static final String ordreHELO(String nomHote)
	{
		return String.format("HELO %s", nomHote);
	}
	
	public static final String ordreEHLO(String nomHote)
	{
		return String.format("EHLO %s", nomHote);
	}
	
	public static final String ordreMAIL(String expediteur)
	{
		return String.format("MAIL FROM: <%s>", expediteur);
	}
	
	public static final String ordreMAIL(String expediteur, long messageSize)
	{
		return String.format("MAIL FROM: <%s> SIZE=%d", expediteur, messageSize);
	}
	
	public static final String ordreRCPT(String destinataire)
	{
		return String.format("RCPT TO: <%s>", destinataire);
	}
	
	public static final String ordreAUTH(String type)
	{
		return String.format("AUTH %s", type);
	}
	
	public static final String ordreSTARTTLS = "STARTTLS";
	
	public static final String ordreQUIT = "QUIT";
	
	public static final String ordreDATA = "DATA";
	
	public static final String ordreFINCOURIER = "\r\n.";
}

/*__________________________________________________________*/
/*   Fin du fichier OrdresESMTP.java
/*__________________________________________________________*/