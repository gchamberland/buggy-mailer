package modele.smtp;
import java.io.BufferedReader;
import java.io.BufferedWriter;

import javax.swing.JFrame;

import vue.DialogueAuthentification;
import vue.DialogueMotDePass;
import modele.mime.Base64Coder;
import modele.smtp.exceptions.AuthentificationFailureException;
import modele.smtp.exceptions.ErrorResponseException;
import controleur.SMTPUtile;

/*__________________________________________________________*/
/**
 */
/*__________________________________________________________*/
public class PlainAuthentification implements AuthentificationMethod
{
	/**  */
	private static final long serialVersionUID = 8559103348592819227L;

	public static final String AUTH_TYPE = "PLAIN";
	
	private String userID = null;
	
	private String password = null;
	
	public PlainAuthentification(String identifiant, String motDePasse)
	{	
		this.userID = identifiant;
		this.password = motDePasse;
	}
	
	public PlainAuthentification(String identifiant)
	{	
		this.userID = identifiant;
	}
	
	/*__________________________________________________________*/
	/**
	 * @param in
	 * @param out
	 * @throws AuthentificationFailureException
	 * @throws Exception
	 * @see modele.smtp.AuthentificationMethod#authentifier(java.io.BufferedWriter, java.io.BufferedReader)
	 */
	@Override
	public void authentifier(BufferedReader in, BufferedWriter out, JFrame owner)
			throws AuthentificationFailureException, Exception
	{
		boolean succes = false;
		
		do
		{
			SMTPUtile.envoyerUnOrdre(out, String.format("%s %s", OrdresESMTP.ordreAUTH(AUTH_TYPE), getAuthString(owner)));
			
			try
			{
				SMTPUtile.lireUneReponseMultiligne(in);
				succes = true;
			}
			catch (ErrorResponseException e)
			{
				if (e.getErrorCode() == 554)
					throw new AuthentificationFailureException(String.format("Echec d'authentification : %s", e.getMessage()));
			}
		}
		while (!succes);
	}
	
	private String getAuthString(JFrame owner)
	{
		String authString;
		
		if (password == null)
		{
			DialogueMotDePass dialogueMotDePass = new DialogueMotDePass(owner, this.userID);
			dialogueMotDePass.setVisible(true);
			
			authString = Base64Coder.encodeString(String.format("\000%s\000%s", this.userID, dialogueMotDePass.getMotDePasse()));
		}
		else
		{
			authString = Base64Coder.encodeString(String.format("\000%s\000%s", this.userID, this.password));
		}
		
		return authString;
	}

	/*__________________________________________________________*/
	/**
	 * @return
	 * @see modele.smtp.AuthentificationMethod#getName()
	 */
	@Override
	public String getNom()
	{
		return AUTH_TYPE;
	}
	
	public String getUserID()
	{
		return userID;
	}
	
}