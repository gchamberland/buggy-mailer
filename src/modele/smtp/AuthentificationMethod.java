/*__________________________________________________________*/
/*__________________________________________________________*/
/**
 * Fichier : AuthentificationMethod.java
 * 
 * créé le : 12 févr. 2014 à 16:33:49
 *
 * Auteur : Grégoire Chamberland
 */
/*__________________________________________________________*/
package modele.smtp;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.Serializable;

import javax.swing.JFrame;

import vue.DialogueAuthentification;
import modele.smtp.exceptions.AuthentificationFailureException;

/*__________________________________________________________*/
/**
 */
/*__________________________________________________________*/
public interface AuthentificationMethod extends Serializable
{
	public void authentifier(BufferedReader in, BufferedWriter out, JFrame owner) throws AuthentificationFailureException, Exception;
	public String getNom();
}

/*__________________________________________________________*/
/*   Fin du fichier AuthentificationMethod.java
/*__________________________________________________________*/