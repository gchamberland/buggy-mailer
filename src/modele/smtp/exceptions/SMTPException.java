/*__________________________________________________________*/
/*__________________________________________________________*/
/**
 * Fichier : SMTPException.java
 * 
 * créé le : 11 févr. 2014 à 14:02:24
 *
 * Auteur : Grégoire Chamberland
 */
/*__________________________________________________________*/
package modele.smtp.exceptions;

/*__________________________________________________________*/
/**
 */
/*__________________________________________________________*/
public class SMTPException extends Exception
{
	/*__________________________________________________________*/
	/**
	 * @param message
	 */
	public SMTPException(String message)
	{
		super(message);
		// A compéter Auto-generated constructor stub
	}
}

/*__________________________________________________________*/
/*   Fin du fichier SMTPException.java
/*__________________________________________________________*/