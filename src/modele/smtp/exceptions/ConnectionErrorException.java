/*__________________________________________________________*/
/*__________________________________________________________*/
/**
 * Fichier : ConnectionError.java
 * 
 * créé le : 20 févr. 2014 à 15:13:47
 *
 * Auteur : Grégoire Chamberland
 */
/*__________________________________________________________*/
package modele.smtp.exceptions;

/*__________________________________________________________*/
/**
 */
/*__________________________________________________________*/
public class ConnectionErrorException extends Exception
{

	/*__________________________________________________________*/
	/**
	 * @param message
	 */
	public ConnectionErrorException(String message)
	{
		super(message);
		// A compéter Auto-generated constructor stub
	}
	
}

/*__________________________________________________________*/
/*   Fin du fichier ConnectionError.java
/*__________________________________________________________*/