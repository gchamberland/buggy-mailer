/*__________________________________________________________*/
/*__________________________________________________________*/
/**
 * Fichier : NotSupportedAuthentification.java
 * 
 * créé le : 12 févr. 2014 à 16:40:41
 *
 * Auteur : Grégoire Chamberland
 */
/*__________________________________________________________*/
package modele.smtp.exceptions;

/*__________________________________________________________*/
/**
 */
/*__________________________________________________________*/
public class NotSupportedAuthentification extends Exception
{
	/*__________________________________________________________*/
	/**
	 * @param message
	 */
	public NotSupportedAuthentification(String message)
	{
		super(message);
	}
	
}

/*__________________________________________________________*/
/*   Fin du fichier NotSupportedAuthentification.java
/*__________________________________________________________*/