/*__________________________________________________________*/
/*__________________________________________________________*/
/**
 * Fichier : ProtocolErrorException.java
 * 
 * créé le : 11 févr. 2014 à 14:16:26
 *
 * Auteur : Grégoire Chamberland
 */
/*__________________________________________________________*/
package modele.smtp.exceptions;

/*__________________________________________________________*/
/**
 */
/*__________________________________________________________*/
public class ProtocolErrorException extends SMTPException
{
	/*__________________________________________________________*/
	/**
	 * @param message
	 */
	public ProtocolErrorException(String message)
	{
		super(message);
		// A compéter Auto-generated constructor stub
	}
}

/*__________________________________________________________*/
/*   Fin du fichier ProtocolErrorException.java
/*__________________________________________________________*/