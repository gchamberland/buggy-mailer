/*__________________________________________________________*/
/*__________________________________________________________*/
/**
 * Fichier : AuthentificationFailureException.java
 * 
 * créé le : 11 févr. 2014 à 14:12:45
 *
 * Auteur : Grégoire Chamberland
 */
/*__________________________________________________________*/
package modele.smtp.exceptions;

/*__________________________________________________________*/
/**
 */
/*__________________________________________________________*/
public class AuthentificationFailureException extends SMTPException
{
	/*__________________________________________________________*/
	/**
	 * @param message
	 */
	public AuthentificationFailureException(String message)
	{
		super(message);
		// A compéter Auto-generated constructor stub
	}
}

/*__________________________________________________________*/
/*   Fin du fichier AuthentificationFailureException.java
/*__________________________________________________________*/