/*__________________________________________________________*/
/*__________________________________________________________*/
/**
 * Fichier : ErrorResponseException.java
 * 
 * créé le : 11 févr. 2014 à 20:27:49
 *
 * Auteur : Grégoire Chamberland
 */
/*__________________________________________________________*/
package modele.smtp.exceptions;

/*__________________________________________________________*/
/**
 */
/*__________________________________________________________*/
public class ErrorResponseException extends SMTPException
{

	private int errorCode;
	
	/*__________________________________________________________*/
	/**
	 * @param message
	 */
	public ErrorResponseException(String message, int errorCode)
	{
		super(String.format("%d - %s", errorCode, message));
		
		this.errorCode = errorCode;
	}

	public int getErrorCode()
	{
		return errorCode;
	}

}

/*__________________________________________________________*/
/*   Fin du fichier ErrorResponseException.java
/*__________________________________________________________*/