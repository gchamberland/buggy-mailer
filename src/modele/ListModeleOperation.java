/*__________________________________________________________*/
/*__________________________________________________________*/
/**
 * Fichier : ListModeleOperation.java
 * 
 * créé le : 15 févr. 2014 à 19:30:39
 *
 * Auteur : Grégoire Chamberland
 */
/*__________________________________________________________*/
package modele;

import java.util.concurrent.locks.ReentrantLock;

import javax.swing.DefaultListModel;
import javax.swing.ListModel;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.ListDataEvent;
import javax.swing.event.ListDataListener;

import com.sun.org.apache.bcel.internal.generic.INSTANCEOF;

import controleur.OperationArrierePlan;

/*__________________________________________________________*/
/**
 */
/*__________________________________________________________*/
public class ListModeleOperation extends ThreadSafeListModel<OperationArrierePlan> implements ChangeListener
{
	ReentrantLock mutex = new ReentrantLock();
	
	public void addElement(OperationArrierePlan element)
	{
		mutex.lock();
		
		element.addChangeListener(this);
		super.add(element);
		
		mutex.unlock();
	}
	
	/*__________________________________________________________*/
	/**
	 * @param index
	 * @param element
	 * @see javax.swing.DefaultListModel#add(int, java.lang.Object)
	 */
	@Override
	public void add(int index, OperationArrierePlan element)
	{
		mutex.lock();
		
		element.addChangeListener(this);
		super.add(index, element);
		
		mutex.unlock();
	}
	
	/*__________________________________________________________*/
	/**
	 * @param index
	 * @return
	 * @see javax.swing.DefaultListModel#remove(int)
	 */
	@Override
	public OperationArrierePlan remove(int index)
	{
		getElementAt(index).removeChangeListener(this);
		return super.remove(index);
	}
	
	/*__________________________________________________________*/
	/**
	 * @param e
	 * @see javax.swing.event.ChangeListener#stateChanged(javax.swing.event.ChangeEvent)
	 */
	@Override
	public void stateChanged(ChangeEvent e)
	{
		int sourceIndex = super.indexOf((OperationArrierePlan)e.getSource());
		
		for (ListDataListener listener : getListDataListeners())
			listener.contentsChanged(new ListDataEvent(this, ListDataEvent.CONTENTS_CHANGED, sourceIndex, sourceIndex));
	}
}

/*__________________________________________________________*/
/*   Fin du fichier ListModeleOperation.java
/*__________________________________________________________*/