/*__________________________________________________________*/
/*__________________________________________________________*/
/**
 * Fichier : ThreadSafeListModel.java
 * 
 * créé le : 14 déc. 2013 à 18:19:48
 *
 * Auteur : Grégoire Chamberland
 */
/*__________________________________________________________*/
package modele;

import java.util.Iterator;
import java.util.Vector;

import javax.swing.ListModel;
import javax.swing.event.ListDataEvent;
import javax.swing.event.ListDataListener;

import controleur.OperationArrierePlan;

/*__________________________________________________________*/
/**Un model de JList synchronisé pour pouvoir être utilisé par plusieurs threads à la fois.
 */
/*__________________________________________________________*/
/*__________________________________________________________*/
/**
 * @param <E>
 */
/*__________________________________________________________*/
public class ThreadSafeListModel<E> implements ListModel, Iterable<E>
{
	/** Un conteneur dans lequel sont stockés observateurs. */
	protected Vector<ListDataListener> listeners = new Vector<ListDataListener>();
	
	/** Un conteneur dans lequel sont stockées les données.  */
	private Vector<E> elements = new Vector<E>();
	
	/*__________________________________________________________*/
	/**Ajoute un observateur à cet objet.
	 * @param listener L'observateur à ajouter.
	 * @see javax.swing.ListModel#addListDataListener(javax.swing.event.ListDataListener)
	 */
	@Override
	public void addListDataListener(ListDataListener listener)
	{
		listeners.add(listener);
	}

	/*__________________________________________________________*/
	/**Obtient un élément à un indice donné.
	 * @param index L'indice de l'élément à récupèrer.
	 * @return L'élément récupèré.
	 * @see javax.swing.ListModel#getElementAt(int)
	 */
	@Override
	public E getElementAt(int index)
	{
		return elements.get(index);
	}

	/*__________________________________________________________*/
	/**Donne le nombre d'élément dans contenu par cet objet.
	 * @return Le nombre d'élément dans contenu par cet objet.
	 * @see javax.swing.ListModel#getSize()
	 */
	@Override
	public int getSize()
	{
		return elements.size();
	}

	/*__________________________________________________________*/
	/**Retire un observateur à cet objet.
	 * @param listener L'observateur à retirer.
	 * @see javax.swing.ListModel#removeListDataListener(javax.swing.event.ListDataListener)
	 */
	@Override
	public void removeListDataListener(ListDataListener listener)
	{
		listeners.remove(listener);
	}
	
	/*__________________________________________________________*/
	/** Permet d'obtenir la valeur du champ listeners
	 * @return La valeur du champ listeners.
	 */
	public Vector<ListDataListener> getListDataListeners()
	{
		return new Vector(listeners);
	}
	
	/*__________________________________________________________*/
	/**Ajoute un élément à cet objet.
	 * @param element L'élément à ajouter.
	 */
	public void add(E element)
	{
		add(getSize(), element);
	}
	
	/*__________________________________________________________*/
	/**Insert un élément à un indice donné.
	 * @param index L'indice où inserer l'élément.
	 * @param element L'élément en question.
	 */
	public void add(int index, E element)
	{
		elements.add(index, element);
		
		for (ListDataListener listener : listeners)
		{
			listener.intervalAdded(new ListDataEvent(this, ListDataEvent.INTERVAL_ADDED, index, index));
		}
	}
	
	/*__________________________________________________________*/
	/**Retire un élément à cet objet.
	 * @param element L'élément à retirer.
	 * @return true si l'élément à été retiré, false sinon.
	 */
	public boolean remove(E element)
	{
		int index = elements.indexOf(element);
		
		boolean result = elements.remove(element);
		
		for (ListDataListener listener : listeners)
		{
			listener.intervalRemoved(new ListDataEvent(this, ListDataEvent.INTERVAL_REMOVED, index, index));
		}
		
		return result;
	}
	
	/*__________________________________________________________*/
	/**Retire un élément à un indice donné.
	 * @param index L'indice de l'élément à retirer.
	 * @return L'élément retiré.
	 */
	public E remove(int index)
	{
		E element = elements.remove(index);
		
		for (ListDataListener listener : listeners)
		{
			listener.intervalRemoved(new ListDataEvent(this, ListDataEvent.INTERVAL_REMOVED, index, index));
		}
		
		return element;
	}

	/*__________________________________________________________*/
	/**Donne un itérateur de cet objet.
	 * @return Un itérateur de cet objet.
	 * @see java.lang.Iterable#iterator()
	 */
	@Override
	public Iterator<E> iterator()
	{
		return elements.iterator();
	}
	
	/*__________________________________________________________*/
	/**Donne l'indice d'un élément donné.
	 * @param element L'élément en question.
	 * @return return L'indice de l'élément passé en paramètre.
	 */
	public int indexOf(E element)
	{
		return elements.indexOf(element);
	}

	/*__________________________________________________________*/
	/**Indique si cet objet est vide.
	 * @return true si l'objet est vide, false sinon.
	 */
	public boolean isEmpty()
	{
		return elements.isEmpty();
	}

	/*__________________________________________________________*/
	/**
	 * @param obj
	 * @return
	 */
	public boolean removeElement(E obj)
	{
		return remove(obj);
	}
}

/*__________________________________________________________*/
/*   Fin du fichier ThreadSafeListModel.java
/*__________________________________________________________*/