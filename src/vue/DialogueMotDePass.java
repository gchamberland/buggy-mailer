/*__________________________________________________________*/
/*__________________________________________________________*/
/**
 * Fichier : DialogueMotDePass.java
 * 
 * créé le : 18 févr. 2014 à 01:08:25
 *
 * Auteur : Grégoire Chamberland
 */
/*__________________________________________________________*/
package vue;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

/*__________________________________________________________*/
/**
 */
/*__________________________________________________________*/
public class DialogueMotDePass extends JDialog implements ActionListener
{
	private JPasswordField champMotDePasse;
	private JButton boutonOk;
	
	public DialogueMotDePass(JFrame owner, String identifiant)
	{
		super(owner, "Authentification");
		
		setModal(true);
		agencerComposants(identifiant);
		pack();
		setLocationRelativeTo(owner);
	}
	
	public void agencerComposants(String identifiant)
	{
		getContentPane().setLayout(new BorderLayout());
		
		champMotDePasse = new JPasswordField();
		
		getContentPane().add(ajouterLabel(String.format("Mot de passe pour %s :", identifiant), champMotDePasse), BorderLayout.CENTER);
		getContentPane().add(creerPanelSud(), BorderLayout.SOUTH);
	}
	
	/*__________________________________________________________*/
	/**
	 * @return
	 */
	private Component creerPanelSud()
	{
		JPanel panelSud = new JPanel(new FlowLayout(FlowLayout.RIGHT));
		
		boutonOk = new JButton("Ok");
		boutonOk.addActionListener(this);
		
		panelSud.add(boutonOk);
		
		return panelSud;
	}

	private JPanel ajouterLabel(String label, Component comp)
	{
		JPanel panel = new JPanel(new GridLayout(2, 1));
		
		panel.add(new JLabel(label));
		panel.add(comp);
		
		return panel;
	}

	/*__________________________________________________________*/
	/**
	 * @param e
	 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
	 */
	@Override
	public void actionPerformed(ActionEvent e)
	{
		if (e.getSource() == boutonOk)
		{
			setVisible(false);
		}
	}

	/*__________________________________________________________*/
	/**
	 * @return
	 */
	public Object getMotDePasse()
	{
		return new String(champMotDePasse.getPassword());
	}
}

/*__________________________________________________________*/
/*   Fin du fichier DialogueMotDePass.java
/*__________________________________________________________*/