/*__________________________________________________________*/
/*__________________________________________________________*/
/**
 * Fichier : PieceJointeRenderer.java
 * 
 * créé le : 14 févr. 2014 à 21:40:06
 *
 * Auteur : Grégoire Chamberland
 */
/*__________________________________________________________*/
package vue;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.io.File;

import javax.swing.BorderFactory;
import javax.swing.DefaultListCellRenderer;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.ListCellRenderer;

import controleur.Controleur;

/*__________________________________________________________*/
/**
 */
/*__________________________________________________________*/
public class PieceJointeRenderer implements
		ListCellRenderer<File>
{
	private static final Color bgColor = new Color(142, 177, 221);
	/*__________________________________________________________*/
	/**
	 * @param list
	 * @param value
	 * @param index
	 * @param isSelected
	 * @param cellHasFocus
	 * @return
	 * @see javax.swing.ListCellRenderer#getListCellRendererComponent(javax.swing.JList, java.lang.Object, int, boolean, boolean)
	 */
	@Override
	public Component getListCellRendererComponent(JList<? extends File> list,
			File value, int index, boolean isSelected, boolean cellHasFocus)
	{
		JPanel panel = new JPanel(new FlowLayout(1, 5, 1));
		
		JLabel nomFichier = new JLabel(value.getName(), JLabel.LEFT);
		nomFichier.setPreferredSize(new Dimension(100, 30));
		
		JLabel tailleFichier = new JLabel(Controleur.getHumanReadable(value.length()), JLabel.RIGHT);
		tailleFichier.setPreferredSize(new Dimension(75, 30));
		
		panel.add(nomFichier);
		panel.add(tailleFichier);
		panel.setPreferredSize(new Dimension(200, 35));
		
		if (isSelected)
		{
			panel.setOpaque(true);
			panel.setBackground(bgColor);
		}
	
		return panel;
	}

}

/*__________________________________________________________*/
/*   Fin du fichier PieceJointeRenderer.java
/*__________________________________________________________*/