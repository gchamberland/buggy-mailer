/*__________________________________________________________*/
/*__________________________________________________________*/
/**
 * Fichier : ColorisedTextPane.java
 * 
 * créé le : 8 févr. 2014 à 18:53:11
 *
 * Auteur : Grégoire Chamberland
 */
/*__________________________________________________________*/
package vue;

import java.awt.Color;
import java.awt.Font;
import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.swing.JTextPane;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.DefaultStyledDocument;
import javax.swing.text.Style;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyleContext;

/*__________________________________________________________*/
/**
 */
/*__________________________________________________________*/
public class HTMLTextPane extends JTextPane
{
	private Style defaultStyle;
	HashMap<Pattern, Style> styles = new HashMap<Pattern, Style>();
	
	public HTMLTextPane()
	{
		super();
		
		this.setDocument(new DefaultStyledDocument()
		{

			/*__________________________________________________________*/
			/**
			 * @param offs
			 * @param str
			 * @param a
			 * @throws BadLocationException
			 * @see javax.swing.text.AbstractDocument#insertString(int, java.lang.String, javax.swing.text.AttributeSet)
			 */
			@Override
			public void insertString(int offs, String str, AttributeSet a)
					throws BadLocationException
			{
				super.insertString(offs, str, a);
				coloriser();
			}

			/*__________________________________________________________*/
			/**
			 * @param offs
			 * @param len
			 * @throws BadLocationException
			 * @see javax.swing.text.AbstractDocument#remove(int, int)
			 */
			@Override
			public void remove(int offs, int len) throws BadLocationException
			{
				super.remove(offs, len);
				coloriser();
			}
		});
		
		creerStylesEtExpression();
	}
	
	private void coloriser()
	{	
		String paneContent = null;
		DefaultStyledDocument d = (DefaultStyledDocument) getDocument();
		
		try
		{
			paneContent = d.getText(0, d.getLength());
		} catch (BadLocationException e)
		{
			return;
		}
		
		d.setCharacterAttributes(0, paneContent.length()-1, defaultStyle, true);
		
		int i = 1;
		
		for (Pattern expression : styles.keySet())
		{
			
			Matcher matcher = expression.matcher(paneContent);
			
			while (matcher.find())
			{
				for (int y=1; y<matcher.groupCount()+1; y++)
				{
					d.setCharacterAttributes(matcher.start(y), matcher.end(y) - matcher.start(y), styles.get(expression), true);
				}
			}
			
			i++;
		}
	}
	
	private void ajouterStyle(String expression, Style style)
	{
		styles.put(Pattern.compile(expression, Pattern.CASE_INSENSITIVE | Pattern.MULTILINE), style);
	}
	
	private Style creerStyle(Color foreground, int fontSize, 
			String fontFamily, boolean underlined, boolean bold, boolean italic)
	{	
		StyleContext sc = new StyleContext();
		
		Style style = sc.addStyle("", null);
		
		style.addAttribute(StyleConstants.Foreground, foreground);
		style.addAttribute(StyleConstants.FontSize, fontSize);
		style.addAttribute(StyleConstants.FontFamily, fontFamily);
		style.addAttribute(StyleConstants.Underline, underlined);
		style.addAttribute(StyleConstants.Bold, bold);
		style.addAttribute(StyleConstants.Italic, italic);
		
		return style;
	}
	
	private void creerStylesEtExpression()
	{
		defaultStyle = creerStyle(Color.BLACK, 11, Font.MONOSPACED, false, false, false);
		
		ajouterStyle("(<).+?(>)", 
				creerStyle(Color.BLUE, 
						(Integer)defaultStyle.getAttribute(StyleConstants.FontSize), 
						(String)defaultStyle.getAttribute(StyleConstants.FontFamily), 
						false, true, false));
		
		ajouterStyle("(?:</?(\\w+).*?>)", 
				creerStyle(new Color(104, 5, 155), 
						(Integer)defaultStyle.getAttribute(StyleConstants.FontSize), 
						(String)defaultStyle.getAttribute(StyleConstants.FontFamily), 
						false, true, false));
		
		ajouterStyle("\\w+=(\"[^\"]*?\")", 
				creerStyle(Color.BLUE, 
						(Integer)defaultStyle.getAttribute(StyleConstants.FontSize), 
						(String)defaultStyle.getAttribute(StyleConstants.FontFamily), 
						false, true, false));
		
		ajouterStyle("(\\w+)=\"[^\"]*?\"", 
				creerStyle(Color.black, 
						(Integer)defaultStyle.getAttribute(StyleConstants.FontSize), 
						(String)defaultStyle.getAttribute(StyleConstants.FontFamily), 
						false, true, false));
		
		ajouterStyle("(&.+;)", 
				creerStyle(Color.orange, 
						(Integer)defaultStyle.getAttribute(StyleConstants.FontSize), 
						(String)defaultStyle.getAttribute(StyleConstants.FontFamily), 
						false, false, false));
	}
}

/*__________________________________________________________*/
/*   Fin du fichier ColorisedTextPane.java
/*__________________________________________________________*/