/*__________________________________________________________*/
/*__________________________________________________________*/
/**
 * Fichier : EditeurDeCourier.java
 * 
 * créé le : 8 févr. 2014 à 16:06:17
 *
 * Auteur : Grégoire Chamberland
 */
/*__________________________________________________________*/
package vue;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.regex.Pattern;

import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.JTextPane;

import modele.smtp.CompteUtilisateur;
import controleur.Controleur;

/*__________________________________________________________*/
/** Un composant graphique permettant d'écrire un courrier simple.
 */
/*__________________________________________________________*/
public class EditeurDeCourrier extends JPanel implements ActionListener
{
	/**  */
	private static final long serialVersionUID = -2532757705898469296L;

	private static final Pattern adresseMailValide = Pattern.compile("[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,4}", Pattern.CASE_INSENSITIVE);
	
	protected static JFileChooser fileChooser = new JFileChooser(System.getProperty("user.home"));
	
	private JTextField champDestinataire;
	private JTextField champCopiesCarbone;
	private JTextField champCopiesCarboneInvisibles;
	protected JTextField champSujet;
	
	private DefaultListModel<File> listePiecesJointes;
	
	private JButton boutonEnvoyer;
	private JButton boutonAjouterPieceJointe;
	private JButton boutonSupprimerPieceJointe;
	
	protected JTextPane zoneDeTexte;
	
	protected JFrame owner;
	
	protected Controleur controleur;
	
	protected CompteUtilisateur compteExpedition;
	
	JList<File> composantListePiecesJointes;
	
	
	/*__________________________________________________________*/
	/** Contruit un éditeur de courrier.
	 * @param owner La fenetre contenant ce composants.
	 * @param controleur Le controleur de l'application.
	 * @param compteExpedition Le compte de messagerie utilisé pourl'envoi du courrier.
	 */
	public EditeurDeCourrier(JFrame owner, Controleur controleur, CompteUtilisateur compteExpedition)
	{
		this.compteExpedition = compteExpedition;
		this.controleur = controleur;
		this.owner = owner;
		listePiecesJointes = new DefaultListModel<File>();
		creerComposants();
		agencerComposants();
	}

	/*__________________________________________________________*/
	/**
	 */
	protected void actionEnvoyer(String[] destinataires,
									String[] destinatairesCopiesCarbone,
									String[] destinatairesCopiesCarboneInvisibles,
									String sujet,
									String texte,
									DefaultListModel<File> piecesJointes)
	{
		controleur.envoyerCourierSimple(compteExpedition,
				destinataires,
				destinatairesCopiesCarbone,
				destinatairesCopiesCarboneInvisibles,
				sujet,
				texte,
				piecesJointes);
	}
	
	/*__________________________________________________________*/
	/**
	 * @param e
	 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
	 */
	@Override
	public void actionPerformed(ActionEvent e)
	{
		if (e.getSource() == boutonAjouterPieceJointe)
		{
			ajouterPieceJointe();
		}
		else if (e.getSource() == boutonSupprimerPieceJointe)
		{
			supprimerPieceJointe();
		}
		else if (e.getSource() == boutonEnvoyer)
		{
			envoi();
		}
		
	}
	
	protected void agencerComposants()
	{
		setLayout(new BorderLayout(2, 2));
		add(creerPanelNord(), BorderLayout.NORTH);
		add(creerPanelCentre(), BorderLayout.CENTER);
		add(creerPanelSud(), BorderLayout.SOUTH);
	}
	
	/*__________________________________________________________*/
	/**
	 */
	private void ajouterPieceJointe()
	{
		File f;
		int result;
		
		fileChooser.setAcceptAllFileFilterUsed(true);
		fileChooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
		fileChooser.setMultiSelectionEnabled(false);
		
		result = fileChooser.showDialog(owner, "Choisir");
		
		f = fileChooser.getSelectedFile();
		
		if (f == null || result != JFileChooser.APPROVE_OPTION)
			return;
		
		if (f.exists() && f.length() > 0)
		{
			listePiecesJointes.addElement(f);
			boutonSupprimerPieceJointe.setEnabled(true);
		}
		else
		{
			JOptionPane.showMessageDialog(owner,
										String.format("Le fichier \"%s\" n'existe pas ou ne contient aucune donnée.",
										f.getAbsolutePath()),
										"Fichier invalide",
										JOptionPane.ERROR_MESSAGE);
		}
	}
	
	protected void creerComposants()
	{
		champDestinataire = new JTextField();
		champCopiesCarbone = new JTextField();
		champCopiesCarboneInvisibles = new JTextField();
		
		champSujet = new JTextField();
		
		zoneDeTexte = new JTextPane();
		
		boutonEnvoyer = new JButton("Envoyer");
		boutonEnvoyer.addActionListener(this);
		
		boutonAjouterPieceJointe = new JButton("Ajouter une pièce jointe");
		boutonAjouterPieceJointe.addActionListener(this);
		
		boutonSupprimerPieceJointe = new JButton("Supprimer la pièce jointe");
		boutonSupprimerPieceJointe.setEnabled(false);
		boutonSupprimerPieceJointe.addActionListener(this);
	}
	
	/*__________________________________________________________*/
	/** Crée le composant central.
	 * @return Une instancede JPanel contenant le composant central.
	 */
	protected JPanel creerPanelCentre()
	{
		JPanel panelCentre = new JPanel(new BorderLayout());
		
		panelCentre.add(new JScrollPane(zoneDeTexte));
		
		return panelCentre;
	}
	
	protected JPanel creerPanelChampAvecLabel(JTextField champ, String label)
	{
		JPanel panel = new JPanel(new BorderLayout(2, 2));
		
		panel.add(new JLabel(label), BorderLayout.WEST);
		
		panel.add(champ, BorderLayout.CENTER);
		
		return panel;
	}

	protected JPanel creerPanelDestinataires()
	{
		JPanel panelDestinataires = new JPanel(new BorderLayout());
		
		JPanel panelLabel = new JPanel(new GridLayout(4, 1, 2, 2));
		JPanel panelChamps = new JPanel(new GridLayout(4, 1, 2, 2));
		
		panelLabel.add(new JLabel("À :", JLabel.RIGHT));
		panelLabel.add(new JLabel("Cc :", JLabel.RIGHT));
		panelLabel.add(new JLabel("Cci :", JLabel.RIGHT));
		panelLabel.add(new JLabel("Sujet :", JLabel.RIGHT));
		
		panelChamps.add(champDestinataire);
		panelChamps.add(champCopiesCarbone);
		panelChamps.add(champCopiesCarboneInvisibles);
		panelChamps.add(champSujet);
		
		panelDestinataires.add(panelLabel, BorderLayout.WEST);
		panelDestinataires.add(panelChamps, BorderLayout.CENTER);
		
		return panelDestinataires;
	}
	
	protected JPanel creerPanelNord()
	{
		JPanel panelNord = new JPanel(new BorderLayout());
		
		JLabel labelExpediteur = new JLabel(String.format("<html>Depuis : <b>%s</b>", compteExpedition.getAdresseMail()), JLabel.CENTER);
		labelExpediteur.setPreferredSize(new Dimension(0, 20));
		
		panelNord.add(labelExpediteur, BorderLayout.NORTH);
		panelNord.add(creerPanelDestinataires(), BorderLayout.CENTER);
		panelNord.add(boutonEnvoyer, BorderLayout.EAST);
		
		return panelNord;
	}

	protected JPanel creerPanelSud()
	{
		JPanel panelSud = new JPanel(new BorderLayout());
		JPanel panelCommandesPiecesJointes = new JPanel(new FlowLayout(FlowLayout.LEFT));
		
		composantListePiecesJointes = new JList<File>(listePiecesJointes);
		composantListePiecesJointes.setLayoutOrientation(JList.HORIZONTAL_WRAP);
		composantListePiecesJointes.setVisibleRowCount(2);
		composantListePiecesJointes.setCellRenderer(new PieceJointeRenderer());
		
		panelCommandesPiecesJointes.add(boutonAjouterPieceJointe);
		panelCommandesPiecesJointes.add(boutonSupprimerPieceJointe);
		
		JScrollPane scrollPane = new JScrollPane(composantListePiecesJointes);
		scrollPane.setPreferredSize(new Dimension(200, 76));
		
		panelSud.add(panelCommandesPiecesJointes, BorderLayout.NORTH);
		panelSud.add(scrollPane, BorderLayout.SOUTH);
		
		return panelSud;
    }

	/*__________________________________________________________*/
	/**
	 */
	private void envoi()
	{
		if (champDestinataire.getText().equals(""))
		{
			JOptionPane.showMessageDialog(owner, "Impossible d'envoyer un courrier sans destinataires !", "Erreur", JOptionPane.ERROR_MESSAGE);
			return;
		}
		
		if (champSujet.getText().equals(""))
		{
			int result = JOptionPane.showConfirmDialog(this, "Envoyer un courrier sans sujet ?", "Sujet vide", JOptionPane.YES_NO_OPTION);
			
			if (result == JOptionPane.NO_OPTION)
				return;
		}
		
		try
		{
			actionEnvoyer(parserAdresses(champDestinataire),
							parserAdresses(champCopiesCarbone),
							parserAdresses(champCopiesCarboneInvisibles),
							champSujet.getText(),
							zoneDeTexte.getText(),
							listePiecesJointes);
		}
		catch (Exception e)
		{
			JOptionPane.showMessageDialog(owner, e.getLocalizedMessage(), "Erreur", JOptionPane.ERROR_MESSAGE);
		}
	}

	private String[] parserAdresses(JTextField champ) throws Exception
	{
		if (!champ.getText().trim().equals(""))
		{
			String[] adresses = champ.getText().split(" ");
			
			for (String adresse : adresses)
			{
				if (!adresseMailValide.matcher(adresse).matches())
					throw new Exception("Merci d'entrer des adresses valides séparées par un espace.");
			}
			
			return adresses;
		}
		else
			return new String[0];
	}

	private void supprimerPieceJointe()
	{
		int i = composantListePiecesJointes.getSelectedIndex();
		
		if (i > -1)
			listePiecesJointes.remove(i);
		
		if (listePiecesJointes.size() == 0)
			boutonSupprimerPieceJointe.setEnabled(false);	
	}
}

/*__________________________________________________________*/
/*   Fin du fichier EditeurDeCourier.java
/*__________________________________________________________*/