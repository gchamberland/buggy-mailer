/*__________________________________________________________*/
/*__________________________________________________________*/
/**
 * Fichier : PanelAutehntification_ID_Pass.java
 * 
 * créé le : 17 févr. 2014 à 22:46:21
 *
 * Auteur : Grégoire Chamberland
 */
/*__________________________________________________________*/
package vue;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.JPanel;

/*__________________________________________________________*/
/**
 */
/*__________________________________________________________*/
public class PanelAutehntification_ID_Pass extends JPanel implements ActionListener
{
	private JTextField champIdentifiant;
	private JPasswordField champMotDePasse;
	private JCheckBox checkBoxEnregistrerPass;

	public PanelAutehntification_ID_Pass()
	{
		agencerComposants();
	}
	
	public PanelAutehntification_ID_Pass(String id)
	{
		this();
		
		champIdentifiant.setText(id);
	}
	
	private void agencerComposants()
	{
		setLayout(new BorderLayout());
		
		champIdentifiant = new JTextField();
		champIdentifiant.setPreferredSize(new Dimension(150, 30));
		
		checkBoxEnregistrerPass = new JCheckBox("Mémoriser le mot de passe");
		checkBoxEnregistrerPass.addActionListener(this);
		
		champMotDePasse = new JPasswordField();
		champMotDePasse.setEnabled(false);
		champIdentifiant.setPreferredSize(new Dimension(150, 30));
		
		add(ajouterLabel("Identifiant :", champIdentifiant), BorderLayout.NORTH);
		add(checkBoxEnregistrerPass, BorderLayout.CENTER);
		add(ajouterLabel("Mot de passe :", champMotDePasse), BorderLayout.SOUTH);
	}
	
	private JPanel ajouterLabel(String label, Component comp)
	{
		JPanel panel = new JPanel(new GridLayout(2, 1));
		
		panel.add(new JLabel(label));
		panel.add(comp);
		
		return panel;
	}

	/*__________________________________________________________*/
	/**
	 * @param e
	 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
	 */
	@Override
	public void actionPerformed(ActionEvent e)
	{
		if (e.getSource() == checkBoxEnregistrerPass)
		{
			champMotDePasse.setEnabled(checkBoxEnregistrerPass.isSelected());
		}
	}
	
	public String getId()
	{
		return champIdentifiant.getText();
	}
	
	public String getPass()
	{
		return new String(champMotDePasse.getPassword());
	}
	
	public boolean savePass()
	{
		return checkBoxEnregistrerPass.isSelected();
	}
}

/*__________________________________________________________*/
/*   Fin du fichier PanelAutehntification_ID_Pass.java
/*__________________________________________________________*/