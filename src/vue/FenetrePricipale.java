/*__________________________________________________________*/
/*__________________________________________________________*/
/**
 * Fichier : EditeurDeCourier.java
 * 
 * créé le : 6 févr. 2014 à 17:30:37
 *
 * Auteur : Grégoire Chamberland
 */
/*__________________________________________________________*/
package vue;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.DefaultListModel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JToolBar;
import javax.swing.ListSelectionModel;
import javax.swing.border.MatteBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import modele.ListModeleAttentive;
import modele.smtp.CompteUtilisateur;
import controleur.Controleur;
import controleur.OperationArrierePlan;

/*__________________________________________________________*/
/** La fenetre principale de l'application.
 */
/*__________________________________________________________*/
public class FenetrePricipale extends JFrame implements ActionListener, ListSelectionListener, ChangeListener
{	
	/** Numéro de sérialisation */
	private static final long serialVersionUID = 1508864073167905504L;
	
	private static final ImageIcon icon = Controleur.getImage("nouveau_courrier.png", 100, 100);
	private static final ImageIcon imageBoutonAjouterServeur = Controleur.getImage("nouveau_serveur.png", 30, 30);
	private static final ImageIcon imageBoutonParametresServeur = Controleur.getImage("parametres_serveur.png", 30, 30);
	private static final ImageIcon imageBoutonSupprimerServeur = Controleur.getImage("supprimer_serveur.png", 30, 30);
	private static final ImageIcon imageBoutonNouveauCourrier = Controleur.getImage("nouveau_courrier.png", 30, 30);
	private static final ImageIcon imageBoutonNouveauCourrierHTML = Controleur.getImage("nouveau_courrierHTML.png", 30, 30);
	private static final ImageIcon imageBoutonFermerOngletCourant = Controleur.getImage("fermer_onglet.png", 30, 30);
	
	private static final Color couleurInterface = new Color(0x202020);
	
	private JButton boutonAjouterServeur;
	private JButton boutonParametresServeur;
	private JButton boutonSupprimerServeur;
	private JButton boutonNouveauCourrier;
	private JButton boutonNouveauCourrierHTML;
	private JButton boutonFermerOngletCourant;
	
	private JTabbedPane tabsPanel;
	
	private DefaultListModel<CompteUtilisateur> listeServeurs;
	private JList<CompteUtilisateur> composantListeServeurs;
	
	private Controleur controleur;
	
	/*__________________________________________________________*/
	/** Crée une nouvelle instance de fenêtre.
	 * @param listeServeurs La liste des comptes de messagerie de l'application.
	 * @param listeOperationArrierePlan La lise des opérations en cours.
	 * @param controleur Le controleur de l'application.
	 */
	public FenetrePricipale(DefaultListModel<CompteUtilisateur> listeServeurs,
			ListModeleAttentive listeOperationArrierePlan, Controleur controleur)
	{
		super("Editeur de courier Buggy");
		super.setIconImage(icon.getImage());
		
		setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		
		this.listeServeurs = listeServeurs;
		this.controleur = controleur;
		
		creerComposants();
		agencerContentPane(listeOperationArrierePlan);
		
		setMinimumSize(new Dimension(600, 685));
		
		ajouterOnglet("Accueil", creerPanelAccueil());
		
		pack();
	}
	
	private void reglerBouton(JButton bouton, ImageIcon image, String toolTipTexte, boolean enabled)
	{
		bouton.setIcon(image);
		bouton.setPreferredSize(new Dimension(40, 40));
		bouton.setToolTipText(toolTipTexte);
		bouton.addActionListener(this);
		bouton.setEnabled(enabled);
	}

	private void creerComposants()
	{	
		boutonAjouterServeur = new JButton();
		reglerBouton(boutonAjouterServeur, imageBoutonAjouterServeur, "Ajouter un serveur SMTP", true);
		
		boutonParametresServeur = new JButton();
		reglerBouton(boutonParametresServeur, imageBoutonParametresServeur, "Définir les paramètres du serveur sélectionné", false);
		
		boutonSupprimerServeur = new JButton();
		reglerBouton(boutonSupprimerServeur, imageBoutonSupprimerServeur, "Supprimer le serveur sélectionné", false);
		
		boutonNouveauCourrier = new JButton();
		reglerBouton(boutonNouveauCourrier, imageBoutonNouveauCourrier, "Nouveau courrier simple", false);
		
		boutonNouveauCourrierHTML = new JButton();
		reglerBouton(boutonNouveauCourrierHTML, imageBoutonNouveauCourrierHTML, "Créer un nouveau courrier HTML", false);

		boutonFermerOngletCourant = new JButton();
		reglerBouton(boutonFermerOngletCourant, imageBoutonFermerOngletCourant, "Fermer l'onglet courant", false);
		
		tabsPanel = new JTabbedPane();
		tabsPanel.addChangeListener(this);
		tabsPanel.setBorder(new MatteBorder(new Insets(0, 1, 0, 0), Color.GRAY));
		tabsPanel.setBackground(couleurInterface);
		tabsPanel.setOpaque(true);
	}
	
	private void agencerContentPane(ListModeleAttentive listeOperations)
	{
		JPanel conteneurPrincipale = new JPanel(new BorderLayout());
		
		conteneurPrincipale.add(tabsPanel, BorderLayout.CENTER);
		conteneurPrincipale.add(creerPanelOuest(listeOperations), BorderLayout.WEST);
		
		getContentPane().add(creerBarreOutils(), BorderLayout.NORTH);
		getContentPane().add(conteneurPrincipale, BorderLayout.CENTER);
	}
	
	private Component creerPanelAccueil()
	{
		return new JLabel("<html><center color=\"white\">Bienvenue,<br/>Pour commencer, ajoutez un <b>serveur</b><br/>puis créez un <b>courrier</b>.<br>Faites glisser le barre d'outils pour la déplacer</center>", JLabel.CENTER);
	}
	
	private JPanel creerPanelOuest(ListModeleAttentive listeOperations)
	{
		JPanel panelOuest = new JPanel(new BorderLayout());
		
		JPanel panelListe = new JPanel(new BorderLayout());
		
		JList<OperationArrierePlan> composantListOperations = new JList<OperationArrierePlan>(listeOperations);
		composantListOperations.setCellRenderer(new OperationRenderer());
		
		composantListeServeurs = new JList<CompteUtilisateur>(this.listeServeurs);
		composantListeServeurs.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		composantListeServeurs.addListSelectionListener(this);
		JScrollPane scrollPaneListeServeurs = new JScrollPane(composantListeServeurs);
	
		panelListe.add(scrollPaneListeServeurs, BorderLayout.CENTER);
		panelListe.add(new JScrollPane(composantListOperations), BorderLayout.SOUTH);
		panelListe.setBorder(BorderFactory.createEmptyBorder(4, 4, 4, 4));
		panelListe.setBackground(couleurInterface);
		panelListe.setOpaque(true);
		
		panelOuest.add(panelListe, BorderLayout.CENTER);
		
		panelOuest.setPreferredSize(new Dimension(200, 500));
		
		return panelOuest;
	}
	
	private JToolBar creerBarreOutils()
	{
		JToolBar barreOutils = new JToolBar(JToolBar.CENTER);
		
		barreOutils.add(boutonAjouterServeur);
		barreOutils.add(boutonParametresServeur);
		barreOutils.add(boutonSupprimerServeur);
		barreOutils.addSeparator();
		barreOutils.add(boutonNouveauCourrier);
		barreOutils.add(boutonNouveauCourrierHTML);
		barreOutils.addSeparator();
		barreOutils.add(boutonFermerOngletCourant);
		
		barreOutils.setBorder(new MatteBorder(1, 1, 1, 1, Color.GRAY));
		barreOutils.setBackground(couleurInterface);
		barreOutils.setOpaque(true);
		
		return barreOutils;
	}
	
	private void ajouterOnglet(String titre, Component contenu)
	{
		tabsPanel.add(titre, contenu);
		tabsPanel.setSelectedIndex(tabsPanel.getTabCount()-1);
	}

	/*__________________________________________________________*/
	/**
	 * @param e
	 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
	 */
	@Override
	public void actionPerformed(ActionEvent e)
	{
		if (e.getSource() == boutonAjouterServeur)
		{
			controleur.ajouterServeur();
		}
		else if (e.getSource() == boutonNouveauCourrier)
		{
			ajouterOnglet("Nouveau courrier", new EditeurDeCourrier(this, controleur, composantListeServeurs.getSelectedValue()));
		}
		else if (e.getSource() == boutonNouveauCourrierHTML)
		{
			ajouterOnglet("Nouveau courier", new EditeurDeCourrierHTML(this, controleur, composantListeServeurs.getSelectedValue()));
		}
		else if (e.getSource() == boutonParametresServeur)
		{
			CompteUtilisateur serveurSelectionne = composantListeServeurs.getSelectedValue();
			
			if (serveurSelectionne == null)
				return;
			
			controleur.modifierServeur(serveurSelectionne);
		}
		else if (e.getSource() == boutonSupprimerServeur)
		{
			CompteUtilisateur serveurSelectionne = composantListeServeurs.getSelectedValue();
						
			if (serveurSelectionne == null)
				return;
			
			int result = JOptionPane.showConfirmDialog(this, 
					String.format("Voulez-vous vraiment supprimer le serveur\n%s",  serveurSelectionne.toString()),
					"Confirmation", JOptionPane.YES_NO_OPTION);
			
			if (result == JOptionPane.YES_OPTION)
				controleur.supprimerServeur(serveurSelectionne);
		}
		else if (e.getSource() == boutonFermerOngletCourant)
		{
			if (tabsPanel.getTabCount() > 1)
			{
				tabsPanel.remove(tabsPanel.getSelectedIndex());
			}
		}
	}

	/*__________________________________________________________*/
	/**
	 * @param e
	 * @see javax.swing.event.ListSelectionListener#valueChanged(javax.swing.event.ListSelectionEvent)
	 */
	@Override
	public void valueChanged(ListSelectionEvent e)
	{
		if (e.getFirstIndex() == -1)
		{
			boutonNouveauCourrier.setEnabled(false);
			boutonNouveauCourrierHTML.setEnabled(false);
			boutonParametresServeur.setEnabled(false);
			boutonSupprimerServeur.setEnabled(false);
		}
		else if (e.getFirstIndex() > -1)
		{
			boutonNouveauCourrier.setEnabled(true);
			boutonNouveauCourrierHTML.setEnabled(true);
			boutonParametresServeur.setEnabled(true);
			boutonSupprimerServeur.setEnabled(true);
		}
		
	}

	/*__________________________________________________________*/
	/**
	 * @param e
	 * @see javax.swing.event.ChangeListener#stateChanged(javax.swing.event.ChangeEvent)
	 */
	@Override
	public void stateChanged(ChangeEvent e)
	{
		if (e.getSource() == tabsPanel)
		{
			if (tabsPanel.getTabCount() > 1)
				boutonFermerOngletCourant.setEnabled(true);
			else
				boutonFermerOngletCourant.setEnabled(false);
		}
	}
}

/*__________________________________________________________*/
/*   Fin du fichier EditeurDeCourier.java
/*__________________________________________________________*/