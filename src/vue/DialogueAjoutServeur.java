/*__________________________________________________________*/
/*__________________________________________________________*/
/**
 * Fichier : FenetreAjoutServeur.java
 * 
 * créé le : 10 févr. 2014 à 03:19:01
 *
 * Auteur : Grégoire Chamberland
 */
/*__________________________________________________________*/
package vue;

import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Frame;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.net.UnknownHostException;
import java.util.List;
import java.util.Set;
import java.util.regex.Pattern;

import javax.swing.BoxLayout;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

import modele.smtp.CompteUtilisateur;
import modele.smtp.CompteUtilisateur.TypeDeSecurite;
import modele.smtp.exceptions.NotSupportedAuthentification;

/*__________________________________________________________*/
/**
 */
/*__________________________________________________________*/
public class DialogueAjoutServeur extends JDialog implements ActionListener, WindowListener, ItemListener
{
	private static Pattern adresseMailValide = Pattern.compile("[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,4}", Pattern.CASE_INSENSITIVE);
	
	private JTextField champAdresse;
	private JTextField champPort;
	private JTextField champAdresseMail;
	private JTextField champNomComplet;
	
	private JComboBox<TypeDeSecurite> comboTypeSecurite;

	private JButton boutonAnnuler, boutonTerminer;
	
	private CompteUtilisateur serveur;
	
	public DialogueAjoutServeur(Frame owner)
	{
		super(owner);
		
		setModal(true);
		
		agencerContentPane();
		pack();
		
		setLocationRelativeTo(owner);
		
		setResizable(false);
		addWindowListener(this);
	}
	
	public DialogueAjoutServeur(Frame owner, CompteUtilisateur serveur)
	{
		this(owner);
		
		definirContenuComposants(serveur);
	}
	
	/*__________________________________________________________*/
	/**
	 */
	private void definirContenuComposants(CompteUtilisateur serveur)
	{
		champAdresse.setText(serveur.getNomHote());
		champPort.setText(String.format("%d", serveur.getPort()));
		comboTypeSecurite.setSelectedItem(serveur.getTypeDeSecurite());
		champAdresseMail.setText(serveur.getAdresseMail());
		champNomComplet.setText(serveur.getNomUtilisateur());
	}

	private void agencerContentPane()
	{			
		getContentPane().setLayout(new BorderLayout());
		getContentPane().add(creerPanelInfos(), BorderLayout.CENTER);
		getContentPane().add(creerPanelBoutons(), BorderLayout.SOUTH);
	}
	
	private JPanel creerPanelBoutons()
	{
		JPanel panelBoutons = new JPanel(new FlowLayout(FlowLayout.RIGHT));
		
		boutonAnnuler = new JButton("Annuler");
		boutonAnnuler.addActionListener(this);
		
		boutonTerminer = new JButton("Terminer");
		boutonTerminer.addActionListener(this);
		
		panelBoutons.add(boutonAnnuler);
		panelBoutons.add(boutonTerminer);
		
		return panelBoutons;
	}

	private JPanel creerPanelInfos()
	{
		JPanel panelInfos = new JPanel(new BorderLayout());
		
		panelInfos.add(creerPanelAdresse(), BorderLayout.NORTH);
		panelInfos.add(creerPanelTypeSecurite(), BorderLayout.CENTER);
		panelInfos.add(creerInfosUtilisateur(), BorderLayout.SOUTH);
		
		return panelInfos;
	}
	
	private JPanel creerPanelTypeSecurite()
	{
		JPanel panelTypeSecurite = new JPanel(new FlowLayout());
		TypeDeSecurite[] types = { TypeDeSecurite.ESMTP_SECURISE, TypeDeSecurite.SMTP_VIA_SSL, TypeDeSecurite.NON_SECURISE };
		
		comboTypeSecurite = new JComboBox<TypeDeSecurite>(types);
		comboTypeSecurite.setPreferredSize(new Dimension(250, 30));
		comboTypeSecurite.addItemListener(this);
		
		panelTypeSecurite.add(ajouterLabel("Type de sécurité :", comboTypeSecurite));
		
		return panelTypeSecurite;
	}
	
	/*__________________________________________________________*/
	/**
	 * @return
	 */
	private JPanel creerPanelAdresse()
	{
		JPanel panelAdresse = new JPanel(new FlowLayout());
		
		champAdresse = new JTextField();
		champAdresse.setPreferredSize(new Dimension(250, 30));
		
		panelAdresse.add(ajouterLabel("Adresse :", champAdresse));
		
		champPort = new JTextField();
		champPort.setPreferredSize(new Dimension(40, 30));
		champPort.setText("587");
		
		panelAdresse.add(ajouterLabel("Port :", champPort));
		
		return panelAdresse;
	}
	
	private JPanel creerInfosUtilisateur()
	{
		JPanel panelInfosUtilisateur = new JPanel(new BorderLayout());
		JPanel panelAdresseMail = new JPanel(new FlowLayout(FlowLayout.CENTER));
		JPanel panelNomComplet = new JPanel(new FlowLayout(FlowLayout.CENTER));
		
		champAdresseMail = new JTextField();
		champAdresseMail.setPreferredSize(new Dimension(200, 30));
		
		champNomComplet = new JTextField();
		champNomComplet.setPreferredSize(new Dimension(200, 30));
		
		panelAdresseMail.add(ajouterLabel("Adresse mail utilisée :", champAdresseMail));
		panelNomComplet.add(ajouterLabel("Votre nom complet (facultatif) :", champNomComplet));
		
		panelInfosUtilisateur.add(panelNomComplet, BorderLayout.NORTH);
		panelInfosUtilisateur.add(panelAdresseMail);
		
		return panelInfosUtilisateur;
	}
	
	private JPanel ajouterLabel(String label, Component comp)
	{
		JPanel panel = new JPanel(new GridLayout(2, 1));
		
		panel.add(new JLabel(label));
		panel.add(comp);
		
		return panel;
	}
	
	private boolean testerChamps()
	{	
		String adresseMail = champAdresseMail.getText();
		String adresse = champAdresse.getText().trim();
		String nomComplet = champNomComplet.getText().trim();
		TypeDeSecurite type = (TypeDeSecurite) comboTypeSecurite.getSelectedItem();
		
		try
		{
			int port = Integer.parseInt(champPort.getText());
			
			if (adresse.equals(""))
			{
				JOptionPane.showMessageDialog(this, "Merci de saisir une adresse.", "Données saisies incomplètes", JOptionPane.ERROR_MESSAGE);
				return false;
			}
			
			if (!adresseMailValide.matcher(adresseMail).matches())
			{
				JOptionPane.showMessageDialog(this, "Merci de saisir une adresse mail valide.", "Données saisies éronées", JOptionPane.ERROR_MESSAGE);
				return false;
			}
			
			serveur = new CompteUtilisateur(adresse, port, type, adresseMail, nomComplet);
			return true;
		}
		catch (NumberFormatException e)
		{
			JOptionPane.showMessageDialog(this, "Numéro de port invalide.", "Données saisies erronées", JOptionPane.ERROR_MESSAGE);
			return false;
		}
	}
	
	public CompteUtilisateur getServeur()
	{
		return serveur;
	}

	/*__________________________________________________________*/
	/**
	 * @param e
	 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
	 */
	@Override
	public void actionPerformed(ActionEvent e)
	{
		if (e.getSource() == boutonAnnuler)
		{
			setVisible(false);
		}
		else if (e.getSource() == boutonTerminer)
		{
			if (testerChamps())
				setVisible(false);
		}
	}

	/*__________________________________________________________*/
	/**
	 * @param e
	 * @see java.awt.event.WindowListener#windowOpened(java.awt.event.WindowEvent)
	 */
	@Override
	public void windowOpened(WindowEvent e)
	{
		// A compéter Auto-generated method stub
		
	}

	/*__________________________________________________________*/
	/**
	 * @param e
	 * @see java.awt.event.WindowListener#windowClosing(java.awt.event.WindowEvent)
	 */
	@Override
	public void windowClosing(WindowEvent e)
	{
		serveur = null;
	}

	/*__________________________________________________________*/
	/**
	 * @param e
	 * @see java.awt.event.WindowListener#windowClosed(java.awt.event.WindowEvent)
	 */
	@Override
	public void windowClosed(WindowEvent e)
	{
		
	}

	/*__________________________________________________________*/
	/**
	 * @param e
	 * @see java.awt.event.WindowListener#windowIconified(java.awt.event.WindowEvent)
	 */
	@Override
	public void windowIconified(WindowEvent e)
	{
		// A compéter Auto-generated method stub
		
	}

	/*__________________________________________________________*/
	/**
	 * @param e
	 * @see java.awt.event.WindowListener#windowDeiconified(java.awt.event.WindowEvent)
	 */
	@Override
	public void windowDeiconified(WindowEvent e)
	{
		// A compéter Auto-generated method stub
		
	}

	/*__________________________________________________________*/
	/**
	 * @param e
	 * @see java.awt.event.WindowListener#windowActivated(java.awt.event.WindowEvent)
	 */
	@Override
	public void windowActivated(WindowEvent e)
	{
		// A compéter Auto-generated method stub
		
	}

	/*__________________________________________________________*/
	/**
	 * @param e
	 * @see java.awt.event.WindowListener#windowDeactivated(java.awt.event.WindowEvent)
	 */
	@Override
	public void windowDeactivated(WindowEvent e)
	{
		// A compéter Auto-generated method stub
		
	}

	/*__________________________________________________________*/
	/**
	 * @param e
	 * @see java.awt.event.ItemListener#itemStateChanged(java.awt.event.ItemEvent)
	 */
	@Override
	public void itemStateChanged(ItemEvent e)
	{
		if (e.getSource() == comboTypeSecurite)
		{
			switch ((TypeDeSecurite)comboTypeSecurite.getSelectedItem())
			{
				case ESMTP_SECURISE:
					champPort.setText("587");
				break;
				
				case SMTP_VIA_SSL:
					champPort.setText("465");
				break;
				
				case NON_SECURISE:
					champPort.setText("25");
				break;
			}
		}
	}
}

/*__________________________________________________________*/
/*   Fin du fichier FenetreAjoutServeur.java
/*__________________________________________________________*/