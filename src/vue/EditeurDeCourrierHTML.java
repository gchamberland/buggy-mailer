/*__________________________________________________________*/
/*__________________________________________________________*/
/**
 * Fichier : EditeurDeCourierHTML.java
 * 
 * créé le : 8 févr. 2014 à 17:03:00
 *
 * Auteur : Grégoire Chamberland
 */
/*__________________________________________________________*/
package vue;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.io.File;
import java.net.MalformedURLException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.swing.DefaultListModel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JColorChooser;
import javax.swing.JEditorPane;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JToolBar;
import javax.swing.filechooser.FileNameExtensionFilter;

import modele.smtp.CompteUtilisateur;
import controleur.Controleur;

/*__________________________________________________________*/
/** Un composant graphique permettant de creer un courrier formatté en HTML.
 */
/*__________________________________________________________*/
public class EditeurDeCourrierHTML extends EditeurDeCourrier implements KeyListener, ActionListener, FocusListener
{
	/** Numéro de sérialisation */
	private static final long serialVersionUID = 2221712776336246468L;
	
	private static final ImageIcon imageBoutonGauche = Controleur.getImage("gauche.png", 20, 20);
	private static final ImageIcon imageBoutonCentre = Controleur.getImage("centre.png", 20, 20);
	private static final ImageIcon imageBoutonDroite = Controleur.getImage("droite.png", 20, 20);
	private static final ImageIcon imageBoutonGras = Controleur.getImage("gras.png", 20, 20);
	private static final ImageIcon imageBoutonJustifier = Controleur.getImage("justifie.png", 20, 20);
	private static final ImageIcon imageBoutonItalique = Controleur.getImage("italique.png", 20, 20);
	private static final ImageIcon imageBoutonSouligne = Controleur.getImage("souligne.png", 20, 20);
	private static final ImageIcon imageBoutonLien = Controleur.getImage("lien.png", 20, 20);
	private static final ImageIcon imageBoutonImage = Controleur.getImage("image.png", 20, 20);
	private static final ImageIcon imageBoutonCouleur = Controleur.getImage("couleur.png", 20, 20);
	
	private static final Pattern exprTitre = Pattern.compile("(?s).*<head>.*<title>(.*)</title>.*</head>.*", Pattern.CASE_INSENSITIVE);
	
	private static final String conenuParDefault = creerContenuParDefault();
	private static final String titreParDefault = "Sans titre";
	
	private JEditorPane apercuCourier;
	
	private JButton boutonGauche;
	private JButton boutonCentre;
	private JButton boutonDroite;
	private JButton boutonJustifie;
	
	private JButton boutonGras;
	private JButton boutonItalique;
	private JButton boutonSouligne;
	
	private JButton boutonLien;
	private JButton boutonImage;
	private JButton boutonCouleur;
	
	private JToolBar barreOutils;
	
	/*__________________________________________________________*/
	/** Crée un nouvel éditeur de courrier HTML.
	 * @param owner La fenêtre dans laquelle est placé ce composant.
	 * @param controleur Le controleur de l'application.
	 * @param serveur Le compte de messagerie qui servira pour l'expédition de ce courrier.
	 */
	public EditeurDeCourrierHTML(JFrame owner, Controleur controleur, CompteUtilisateur serveur)
	{
		super(owner, controleur, serveur);
		
		zoneDeTexte.setText(conenuParDefault);
		setTitre(titreParDefault);
		apercuCourier.setText(zoneDeTexte.getText());
	}


	protected static String creerContenuParDefault()
	{
		StringBuffer str = new StringBuffer();
		
		str.append("<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 3.2 Final//EN\">");
		str.append("\n<html>\n\t<head>");
		str.append("\n\t\t<meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">");
		str.append("\n\t\t<meta name=\"Generator\" content=\"Buggy/1.0\">\n\t\t<title></title>");
		str.append("\n\t</head>\n\t<body>");
		str.append("\n\n\t\t<div align=\"center\">\n\t\t\t<h1>Votre titre</h1>\n\t\t</div>");
		str.append("\n\t\t<h2>Votre sous-titre</h2>");
		str.append("\n\t\t<p>Votre texte</p>");
		str.append("\n\n\t</body>\n</html>");
		
		return str.toString();
	}
	
	@Override
	protected JPanel creerPanelCentre()
	{
		JPanel panelCentre = new JPanel(new BorderLayout());
		
		JSplitPane splitPane = new JSplitPane(JSplitPane.VERTICAL_SPLIT, new JScrollPane(zoneDeTexte), new JScrollPane(apercuCourier));
		
		splitPane.setResizeWeight(0.5);
		
		panelCentre.add(creerBarreOutils(), BorderLayout.NORTH);
		panelCentre.add(splitPane, BorderLayout.CENTER);
		
		return panelCentre;
	}

	protected void reglerBouton(JButton bouton, ImageIcon image, String toolTipTexte, boolean enabled)
	{
		bouton.setIcon(image);
		bouton.setPreferredSize(new Dimension(30, 30));
		bouton.setToolTipText(toolTipTexte);
		bouton.addActionListener(this);
		bouton.setEnabled(enabled);
	}
	
	protected JToolBar creerBarreOutils()
	{
		barreOutils = new JToolBar();
		
		barreOutils.add(boutonGauche);
		barreOutils.add(boutonCentre);
		barreOutils.add(boutonDroite);
		barreOutils.add(boutonJustifie);
		
		barreOutils.addSeparator();
		
		barreOutils.add(boutonGras);
		barreOutils.add(boutonItalique);
		barreOutils.add(boutonSouligne);
		
		barreOutils.addSeparator();
		
		barreOutils.add(boutonLien);
		barreOutils.add(boutonImage);
		barreOutils.add(boutonCouleur);
		
		return barreOutils;
	}

	/*__________________________________________________________*/
	/**
	 * @see vue.EditeurDeCourrier#creerComposants()
	 */
	@Override
	protected void creerComposants()
	{	
		super.creerComposants();
		
		boutonGauche = new JButton();
		reglerBouton(boutonGauche, imageBoutonGauche, "Aligner le texte sélectionné à gauche.", false);
		
		boutonCentre = new JButton();
		reglerBouton(boutonCentre, imageBoutonCentre, "Aligner le texte sélectionné au centre.", false);

		boutonDroite = new JButton();
		reglerBouton(boutonDroite, imageBoutonDroite, "Aligner le texte sélectionné à droite.", false);
		
		boutonJustifie = new JButton();
		reglerBouton(boutonJustifie, imageBoutonJustifier, "Justifier le texte selectionné", false);
		
		boutonGras = new JButton();
		reglerBouton(boutonGras, imageBoutonGras, "Mettre le texte sélectionné en gras.", false);
		
		boutonItalique = new JButton();
		reglerBouton(boutonItalique, imageBoutonItalique, "Mettre le texte sélectionné en italique.", false);
		
		boutonSouligne = new JButton();
		reglerBouton(boutonSouligne, imageBoutonSouligne, "Souligner le texte sélectionné.", false);
		
		boutonLien = new JButton();
		reglerBouton(boutonLien, imageBoutonLien, "Convertir le texte sélectionné en un lien hypertexte.", false);
		
		boutonImage = new JButton();
		reglerBouton(boutonImage, imageBoutonImage, "Insérer une image.", false);
		
		boutonCouleur = new JButton();
		reglerBouton(boutonCouleur, imageBoutonCouleur, "Changer la couleur du texte sélectionné.", false);
		
		apercuCourier = new JEditorPane("text/html", "");
		apercuCourier.getDocument().putProperty("IgnoreCharsetDirective", true);
		apercuCourier.setEditable(false);
		apercuCourier.addFocusListener(this);
		
		zoneDeTexte = new HTMLTextPane();
		zoneDeTexte.addKeyListener(this);
		zoneDeTexte.addFocusListener(this);
		
		champSujet.addKeyListener(this);
	}
	
	/*__________________________________________________________*/
	/**
	 * @param arg0
	 * @see java.awt.event.KeyListener#keyPressed(java.awt.event.KeyEvent)
	 */
	@Override
	public void keyPressed(KeyEvent arg0)
	{
		// A compéter Auto-generated method stub
		
	}

	/*__________________________________________________________*/
	/**
	 * @param arg0
	 * @see java.awt.event.KeyListener#keyReleased(java.awt.event.KeyEvent)
	 */
	@Override
	public void keyReleased(KeyEvent e)
	{
		if (e.getSource() == zoneDeTexte)
		{
			apercuCourier.setText(zoneDeTexte.getText());
		}
		else if (e.getSource() == champSujet)
		{
			String sujet = champSujet.getText().trim();
			
			if (sujet.length() == 0)
				setTitre(titreParDefault);
			else
				setTitre(sujet);
		}
	}

	/*__________________________________________________________*/
	/**
	 * @param arg0
	 * @see java.awt.event.KeyListener#keyTyped(java.awt.event.KeyEvent)
	 */
	@Override
	public void keyTyped(KeyEvent arg0)
	{
		// A compéter Auto-generated method stub
		
	}
	
	
	
	private void alignerAuCentre()
	{
		entourerAvecBalises(zoneDeTexte.getSelectionStart(), zoneDeTexte.getSelectionEnd(), "<div align=\"center\">", "</div>");
	}
	
	private void alignerAGauche()
	{
		entourerAvecBalises(zoneDeTexte.getSelectionStart(), zoneDeTexte.getSelectionEnd(), "<div align=\"left\">", "</div>");
	}
	
	private void alignerADroite()
	{
		entourerAvecBalises(zoneDeTexte.getSelectionStart(), zoneDeTexte.getSelectionEnd(), "<div align=\"right\">", "</div>");
	}
	
	private void justifier()
	{
		entourerAvecBalises(zoneDeTexte.getSelectionStart(), zoneDeTexte.getSelectionEnd(), "<div align=\"justify\">", "</div>");
	}
	
	private void mettreEnCouleur()
	{	
		Color c = JColorChooser.showDialog(this, "Couleur du texte", Color.BLACK);
		
		if (c == null)
			return;
		
		entourerAvecBalises(zoneDeTexte.getSelectionStart(), zoneDeTexte.getSelectionEnd(), String.format("<font color=\"#%02X%02X%02X\">", c.getRed(), c.getGreen(), c.getBlue()), "</font>");
	}
	
	private void mettreEnGras()
	{
		entourerAvecBalises(zoneDeTexte.getSelectionStart(), zoneDeTexte.getSelectionEnd(), "<b>", "</b>");
	}
	
	private void souligne()
	{
		entourerAvecBalises(zoneDeTexte.getSelectionStart(), zoneDeTexte.getSelectionEnd(), "<u>", "</u>");
	}
	
	private void mettreEnItalic()
	{
		entourerAvecBalises(zoneDeTexte.getSelectionStart(), zoneDeTexte.getSelectionEnd(), "<i>", "</i>");
	}
	
	private void insererImage()
	{
		File f;
		int result, height, width;
		float percent;
		String strResult;
		
		fileChooser.setMultiSelectionEnabled(false);
		fileChooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
		fileChooser.setFileFilter(new FileNameExtensionFilter("Fichiers image", "jpg", "jpeg", "png", "gif", "bmp", "svg", "tiff"));
		
		result = fileChooser.showDialog(owner, "Choisir");
		
		f = (fileChooser.getSelectedFile());
		
		if (f == null || result != JFileChooser.APPROVE_OPTION)
			return;
		
		if (f.exists() && f.length() > 0)
		{
			ImageIcon image = new ImageIcon(f.getAbsolutePath());
			
			width = image.getIconWidth();
			height = image.getIconHeight();
			
			if (width > 0 && height > 0)
			{
				strResult = JOptionPane.showInputDialog(owner,
						String.format("L'image mesure %sx%s pixels.\nRedimensionnement (%%) :",	width, height),
						"100");
				
				try
				{	
					if (strResult == null)
						percent = 100;
					else
						percent = Float.parseFloat(strResult);
					
					if (percent > 0 && percent < 101)
					{
						width = (int)  (width / 100.0 * percent);
						height = (int) (height / 100.0 * percent);
						
						insererTexte(zoneDeTexte.getCaretPosition(), String.format("<img width=\"%d\" height=\"%d\" src=\"%s\"/>", width, height, f.toURI().toURL()));
					}
					else
					{
						JOptionPane.showMessageDialog(owner, "Valeur de redimensionnement invalide !", "Erreur", JOptionPane.ERROR_MESSAGE);
					}
				}
				catch (NumberFormatException e)
				{
					JOptionPane.showMessageDialog(owner, "Valeur de redimensionnement invalide !", "Erreur", JOptionPane.ERROR_MESSAGE);
				} catch (MalformedURLException e)
				{}
			}
			else
			{
				JOptionPane.showMessageDialog(owner, "L'image est corrompue ou son format n'est pas supporté !", "Erreur", JOptionPane.ERROR_MESSAGE);
			}
		}
		else
		{
			JOptionPane.showMessageDialog(owner,
										String.format("Le fichier \"%s\" n'existe pas ou ne contient aucune donnée.",
										f.getAbsolutePath()),
										"Fichier invalide",
										JOptionPane.ERROR_MESSAGE);
		}
	}
	
	private void transformerEnLien()
	{
		String lien = JOptionPane.showInputDialog(owner, "Adresse URL :", "Transformer la selection en lien", JOptionPane.QUESTION_MESSAGE);
		
		entourerAvecBalises(zoneDeTexte.getSelectionStart(), zoneDeTexte.getSelectionEnd(), String.format("<a href=\"%s\">", lien), "</a>");
	}

	private void entourerAvecBalises(int selectionStart, int selectionEnd,
			String balise1, String balise2)
	{
		StringBuffer str = new StringBuffer(zoneDeTexte.getText());
		int l1 = balise1.length();
		
		str.insert(selectionStart, balise1);
		str.insert(selectionEnd + l1, balise2);
		
		zoneDeTexte.setText(str.toString());
		apercuCourier.setText(zoneDeTexte.getText());
		
		zoneDeTexte.grabFocus();
		zoneDeTexte.setSelectionStart(selectionStart + l1);
		zoneDeTexte.setSelectionEnd(selectionEnd + l1);
	}
	
	private void insererTexte(int position, String texte)
	{
		StringBuffer str = new StringBuffer(zoneDeTexte.getText());
		
		str.insert(position, texte);
		
		zoneDeTexte.setText(str.toString());
		apercuCourier.setText(zoneDeTexte.getText());
		
		zoneDeTexte.grabFocus();
		zoneDeTexte.setCaretPosition(position + texte.length());
	}
	
	private void setTitre(String titre)
	{
		StringBuffer str = new StringBuffer(zoneDeTexte.getText());
		
		Matcher m = exprTitre.matcher(zoneDeTexte.getText());
		
		if (m.find())
		{
			str.replace(m.start(1), m.end(1), titre);
			zoneDeTexte.setText(str.toString());
		}
	}

	/*__________________________________________________________*/
	/**
	 * @param e
	 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
	 */
	@Override
	public void actionPerformed(ActionEvent e)
	{
		JButton source = (JButton) e.getSource();
		
		if (source == boutonCentre)
		{
			alignerAuCentre();
		}
		else if (source == boutonDroite)
		{
			alignerADroite();
		}
		else if (source == boutonGauche)
		{
			alignerAGauche();
		}
		else if (source == boutonJustifie)
		{
			justifier();
		}
		else if (source == boutonCouleur)
		{
			mettreEnCouleur();
		}
		else if (source == boutonGras)
		{
			mettreEnGras();
		}
		else if (source == boutonSouligne)
		{
			souligne();
		}
		else if (source == boutonItalique)
		{
			mettreEnItalic();
		}
		else if (source == boutonLien)
		{
			transformerEnLien();
		}
		else if (source == boutonImage)
		{
			insererImage();
		}
		
		super.actionPerformed(e);
	}

	/*__________________________________________________________*/
	/**
	 * @param destinataires
	 * @param destinatairesCopiesCarbone
	 * @param destinatairesCopiesCarboneInvisibles
	 * @param sujet
	 * @param texte
	 * @param piecesJointes
	 * @see vue.EditeurDeCourrier#actionEnvoyer(java.lang.String[], java.lang.String[], java.lang.String[], java.lang.String, java.lang.String, javax.swing.DefaultListModel)
	 */
	@Override
	protected void actionEnvoyer(String[] destinataires,
			String[] destinatairesCopiesCarbone,
			String[] destinatairesCopiesCarboneInvisibles, String sujet,
			String texte, DefaultListModel<File> piecesJointes)
	{
		controleur.envoyerCourierHTML(compteExpedition, 
				destinataires, 
				destinatairesCopiesCarbone, 
				destinatairesCopiesCarboneInvisibles, 
				sujet, 
				texte, 
				piecesJointes);
	}

	/*__________________________________________________________*/
	/**
	 * @param e
	 * @see java.awt.event.FocusListener#focusGained(java.awt.event.FocusEvent)
	 */
	@Override
	public void focusGained(FocusEvent e)
	{
		if (e.getSource() == zoneDeTexte)
		{
			for (Component comp : barreOutils.getComponents())
				comp.setEnabled(true);
		}
		else if (e.getSource() == apercuCourier)
		{
			for (Component comp : barreOutils.getComponents())
				comp.setEnabled(false);
		}
	}

	/*__________________________________________________________*/
	/**
	 * @param e
	 * @see java.awt.event.FocusListener#focusLost(java.awt.event.FocusEvent)
	 */
	@Override
	public void focusLost(FocusEvent e)
	{}
}

/*__________________________________________________________*/
/*   Fin du fichier EditeurDeCourierHTML.java
/*__________________________________________________________*/