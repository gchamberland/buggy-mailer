/*__________________________________________________________*/
/*__________________________________________________________*/
/**
 * Fichier : DialogueAuthentification.java
 * 
 * créé le : 15 févr. 2014 à 19:09:26
 *
 * Auteur : Grégoire Chamberland
 */
/*__________________________________________________________*/
package vue;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

import controleur.Controleur;
import modele.smtp.AuthentificationMethod;
import modele.smtp.PlainAuthentification;
import modele.smtp.CompteUtilisateur;
import modele.smtp.exceptions.NotSupportedAuthentification;

/*__________________________________________________________*/
/**
 */
/*__________________________________________________________*/
public class DialogueAuthentification extends JDialog implements ActionListener, ItemListener, WindowListener
{
	private JCheckBox checkBoxAuthrequise;
	private JComboBox<String> comboAuthType;
	
	private JPanel panelInfos;
	
	private JButton boutonTerminer;
	private JButton boutonAnnuler;
	
	private CompteUtilisateur serveur = null;

	public DialogueAuthentification(JFrame owner, CompteUtilisateur serveur)
	{
		super(owner, "Authentification");
		
		this.addWindowListener(this);
		
		AuthentificationMethod auth = serveur.getMetodeAuthentification();
		
		setModal(true);
		this.serveur = serveur;
		
		agencerComposant();
		
		if (auth != null)
		{
			checkBoxAuthrequise.setSelected(true);
			comboAuthType.setEnabled(true);
			comboAuthType.setSelectedItem(auth.getNom());
			reglerContentPanePour(auth);
		}
		
		pack();
		setLocationRelativeTo(owner);
	}
	
	private void reglerContentPanePour(String authTypeName)
	{
		switch (authTypeName)
		{
			case "PLAIN":
				panelInfos = new PanelAutehntification_ID_Pass();
				getContentPane().add(panelInfos, BorderLayout.CENTER);
			break;
			
			case "LOGIN":
				panelInfos = new PanelAutehntification_ID_Pass();
				getContentPane().add(panelInfos, BorderLayout.CENTER);
			break;
		}
		
		pack();
	}
	
	private void reglerContentPanePour(AuthentificationMethod auth)
	{
		switch (auth.getNom())
		{
			case "PLAIN":
				panelInfos = new PanelAutehntification_ID_Pass(((PlainAuthentification) auth).getUserID());
				getContentPane().add(panelInfos, BorderLayout.CENTER);
			break;
			
			case "LOGIN":
				panelInfos = new PanelAutehntification_ID_Pass();
				getContentPane().add(panelInfos, BorderLayout.CENTER);
			break;
		}
		
		pack();
	}
	
	private AuthentificationMethod creerAuthMethode(String authTypeName)
	{
		AuthentificationMethod auth = null;
		
		if (authTypeName.equals("PLAIN"))
		{
			PanelAutehntification_ID_Pass infos = (PanelAutehntification_ID_Pass) panelInfos;
			
			if (infos.savePass())
				auth = new PlainAuthentification(infos.getId(), infos.getPass());
			else
				auth = new PlainAuthentification(infos.getId());
		}
		
		return auth;
	}
	
	private void agencerComposant()
	{
		getContentPane().setLayout(new BorderLayout());
		getContentPane().add(creerPanelNord(), BorderLayout.NORTH);
		getContentPane().add(creerPanelSud(), BorderLayout.SOUTH);
	}
	
	private JPanel creerPanelNord()
	{
		JPanel panelNord = new JPanel(new BorderLayout());
		
		comboAuthType = creerComboAuthType();
		
		checkBoxAuthrequise = creerCheckBoxAuthRequise();
		
		panelNord.add(checkBoxAuthrequise, BorderLayout.NORTH);
		panelNord.add(comboAuthType, BorderLayout.SOUTH);
		
		return panelNord;
	}
	
	private JPanel creerPanelSud()
	{
		JPanel panelSud = new JPanel(new FlowLayout(FlowLayout.RIGHT));
		
		boutonTerminer = new JButton("Terminer");
		boutonTerminer.addActionListener(this);
		
		boutonAnnuler = new JButton("Annuler");
		
		panelSud.add(boutonAnnuler);
		panelSud.add(boutonTerminer);
		
		return panelSud;
	}
	
	private JCheckBox creerCheckBoxAuthRequise()
	{
		JCheckBox checkBox = new JCheckBox("Le serveur requiert une authentification");
		checkBox.addActionListener(this);
		
		return checkBox;
	}
	
	private JComboBox<String> creerComboAuthType()
	{
		JComboBox<String> comboAuthType = new JComboBox<String>(Controleur.getTypeAuthSupportes());
		comboAuthType.setPreferredSize(new Dimension(150, 30));
		comboAuthType.addItemListener(this);
		comboAuthType.setEnabled(false);
		
		return comboAuthType;
	}
	
	private JPanel ajouterLabel(String label, Component comp)
	{
		JPanel panel = new JPanel(new GridLayout(2, 1));
		
		panel.add(new JLabel(label));
		panel.add(comp);
		
		return panel;
	}
	
	public CompteUtilisateur getServeur()
	{
		return serveur;
	}
	
	/*__________________________________________________________*/
	/**
	 * @param e
	 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
	 */
	@Override
	public void actionPerformed(ActionEvent e)
	{
		if (e.getSource() == checkBoxAuthrequise)
		{
			if (checkBoxAuthrequise.isSelected())
			{
				comboAuthType.setEnabled(true);
				reglerContentPanePour((String)comboAuthType.getSelectedItem());
			}
		}
		else if (e.getSource() == boutonTerminer)
		{
			if (checkBoxAuthrequise.isSelected())
			{
				serveur.setAuthentificationMethod(creerAuthMethode((String)comboAuthType.getSelectedItem()));
				setVisible(false);
			}
		}
		else if (e.getSource() == boutonAnnuler)
		{
			serveur = null;
			setVisible(false);
		}
	}
	
	/*__________________________________________________________*/
	/**
	 * @param e
	 * @see java.awt.event.ItemListener#itemStateChanged(java.awt.event.ItemEvent)
	 */
	@Override
	public void itemStateChanged(ItemEvent e)
	{
		if (e.getSource() == comboAuthType)
		{
			String authName = (String)comboAuthType.getSelectedItem();
			
			if (serveur.getTypeAuthentificationSupportees().contains(authName))
				reglerContentPanePour(authName);
			else
				JOptionPane.showMessageDialog(this, "Ce type d'authentification n'est pas supporté par le serveur.");
		}
		
	}

	/*__________________________________________________________*/
	/**
	 * @param e
	 * @see java.awt.event.WindowListener#windowOpened(java.awt.event.WindowEvent)
	 */
	@Override
	public void windowOpened(WindowEvent e)
	{
		// A compéter Auto-generated method stub
		
	}

	/*__________________________________________________________*/
	/**
	 * @param e
	 * @see java.awt.event.WindowListener#windowClosing(java.awt.event.WindowEvent)
	 */
	@Override
	public void windowClosing(WindowEvent e)
	{
		serveur = null;
	}

	/*__________________________________________________________*/
	/**
	 * @param e
	 * @see java.awt.event.WindowListener#windowClosed(java.awt.event.WindowEvent)
	 */
	@Override
	public void windowClosed(WindowEvent e)
	{
		// A compéter Auto-generated method stub
		
	}

	/*__________________________________________________________*/
	/**
	 * @param e
	 * @see java.awt.event.WindowListener#windowIconified(java.awt.event.WindowEvent)
	 */
	@Override
	public void windowIconified(WindowEvent e)
	{
		// A compéter Auto-generated method stub
		
	}

	/*__________________________________________________________*/
	/**
	 * @param e
	 * @see java.awt.event.WindowListener#windowDeiconified(java.awt.event.WindowEvent)
	 */
	@Override
	public void windowDeiconified(WindowEvent e)
	{
		// A compéter Auto-generated method stub
		
	}

	/*__________________________________________________________*/
	/**
	 * @param e
	 * @see java.awt.event.WindowListener#windowActivated(java.awt.event.WindowEvent)
	 */
	@Override
	public void windowActivated(WindowEvent e)
	{
		// A compéter Auto-generated method stub
		
	}

	/*__________________________________________________________*/
	/**
	 * @param e
	 * @see java.awt.event.WindowListener#windowDeactivated(java.awt.event.WindowEvent)
	 */
	@Override
	public void windowDeactivated(WindowEvent e)
	{
		// A compéter Auto-generated method stub
		
	}
}

/*__________________________________________________________*/
/*   Fin du fichier DialogueAuthentification.java
/*__________________________________________________________*/