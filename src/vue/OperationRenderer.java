/*__________________________________________________________*/
/*__________________________________________________________*/
/**
 * Fichier : OperationRenderer.java
 * 
 * créé le : 15 févr. 2014 à 19:27:09
 *
 * Auteur : Grégoire Chamberland
 */
/*__________________________________________________________*/
package vue;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;

import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.ListCellRenderer;

import controleur.OperationArrierePlan;

/*__________________________________________________________*/
/**
 */
/*__________________________________________________________*/
public class OperationRenderer implements
		ListCellRenderer<OperationArrierePlan>
{

	/*__________________________________________________________*/
	/**
	 * @param list
	 * @param value
	 * @param index
	 * @param isSelected
	 * @param cellHasFocus
	 * @return
	 * @see javax.swing.ListCellRenderer#getListCellRendererComponent(javax.swing.JList, java.lang.Object, int, boolean, boolean)
	 */
	@Override
	public Component getListCellRendererComponent(
			JList<? extends OperationArrierePlan> list,
			OperationArrierePlan value, int index, boolean isSelected,
			boolean cellHasFocus)
	{
		JPanel panel = new JPanel(new BorderLayout());
		
		panel.add(new JLabel(String.format("<html><b>%s</b>", value.getDescription()), JLabel.CENTER), BorderLayout.NORTH);
		panel.add(new JProgressBar(value.getProgressionModele()), BorderLayout.CENTER);
		panel.add(new JLabel(value.getEtape(), JLabel.LEFT), BorderLayout.SOUTH);
		
		panel.setPreferredSize(new Dimension(100, 60));
		
		return panel;
	}

}

/*__________________________________________________________*/
/*   Fin du fichier OperationRenderer.java
/*__________________________________________________________*/