/*__________________________________________________________*/
/*__________________________________________________________*/
/**
 * Fichier : BufferedWriterCompteur.java
 * 
 * créé le : 18 févr. 2014 à 02:58:55
 *
 * Auteur : Grégoire Chamberland
 */
/*__________________________________________________________*/
package controleur;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.Writer;

/*__________________________________________________________*/
/**
 */
/*__________________________________________________________*/
public class BufferedWriterCompteur extends BufferedWriter
{
	private OperationArrierePlan representation;
	private int startPercent;
	private int endPercent;
	
	private int lastPercent;
	
	private long writedBytes;
	private long totalBytesToWrite;
	
	/**
         * 
         * @param out
         * @param representation
         * @param startPercent
         * @param endPercent
         * @param totalBytesToWrite 
         */
	public BufferedWriterCompteur(Writer out, OperationArrierePlan representation, int startPercent, int endPercent, long totalBytesToWrite)
	{
		super(out);
		
		this.representation = representation;
		this.startPercent = startPercent;
		this.endPercent = endPercent;
		this.totalBytesToWrite = totalBytesToWrite;
		this.lastPercent = startPercent;
		this.writedBytes = 0;
	}

	private void computeProgression(long nbByesWrited)
	{
		writedBytes += nbByesWrited;
		
		int progression =  (int) (startPercent + ((float)writedBytes) / totalBytesToWrite * (endPercent - startPercent));
		
		if (progression > lastPercent)
		{
			lastPercent = progression;
			representation.setProgression(progression);
		}
	}

	/*__________________________________________________________*/
	/**
	 * @param cbuf
	 * @throws IOException
	 * @see java.io.Writer#write(char[])
	 */
	@Override
	public void write(char[] cbuf) throws IOException
	{
		computeProgression(cbuf.length);
		super.write(cbuf);
	}

	/*__________________________________________________________*/
	/**
	 * @param str
	 * @throws IOException
	 * @see java.io.Writer#write(java.lang.String)
	 */
	@Override
	public void write(String str) throws IOException
	{
		computeProgression(str.length());
		super.write(str);
	}

}

/*__________________________________________________________*/
/*   Fin du fichier BufferedWriterCompteur.java
/*__________________________________________________________*/