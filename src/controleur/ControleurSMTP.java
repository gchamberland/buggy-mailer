/*__________________________________________________________*/
/*__________________________________________________________*/
/**
 * Fichier : ControleurSMTP.java
 * 
 * créé le : 15 févr. 2014 à 17:48:00
 *
 * Auteur : Grégoire Chamberland
 */
/*__________________________________________________________*/
package controleur;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.Socket;
import java.net.URL;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.net.ssl.SSLSocketFactory;
import javax.swing.DefaultListModel;
import javax.swing.ImageIcon;
import javax.swing.JFrame;

import vue.FenetrePricipale;
import modele.Courrier;
import modele.mime.ContenuMIME.ContentDisposition;
import modele.mime.ContenuMIME.ContentEncoding;
import modele.mime.ContenuMIMEMultiParties;
import modele.mime.MessageMIMEHTMLEtImages;
import modele.mime.MessageMIMESimple;
import modele.mime.PieceJointeMIME;
import modele.smtp.OrdresESMTP;
import modele.smtp.CompteUtilisateur;
import modele.smtp.exceptions.AuthentificationFailureException;
import modele.smtp.exceptions.ConnectionErrorException;
import modele.smtp.exceptions.ErrorResponseException;
import modele.smtp.exceptions.ProtocolErrorException;
import modele.smtp.exceptions.SMTPException;

class ControleurSMTP
{
	/** Socket au travers de laquelle on va communiquer avec le serveur. */
	private Socket mSock;
	
	/** Décorateur du flux d'entrée de la socket. */
	private BufferedReader in;
	
	/** Décorateur du flux d'entrée de la socket. */
	private BufferedWriter out;
	
	private CompteUtilisateur serveur;
	
	
	private void decorerFluxES()
			throws Exception
	{
		in = new BufferedReader(new InputStreamReader(mSock.getInputStream()));
		out = new BufferedWriter(new OutputStreamWriter(mSock.getOutputStream(), "US-ASCII"));
	}
	
	private void connecterEnModeNonSecurise(String nomHote, int port)
			throws UnknownHostException, Exception
	{
		mSock = new Socket(nomHote, port);
		
		decorerFluxES();
		
		try
		{
			SMTPUtile.lireLaReponse(in);
		}
		catch (ErrorResponseException e)
		{
			fermerLaConnection();
			throw new Exception(String.format("L'hôte %s est indisponible : %s", nomHote, e.getMessage()));
		}
	}
	
	private void connecterEnModeSMTP_VIA_SSL(String nomHote, int port)
			throws UnknownHostException, Exception
	{
		mSock = SSLSocketFactory.getDefault().createSocket(nomHote, port);
		
		decorerFluxES();
		
		try
		{
			SMTPUtile.lireLaReponse(in);
		}
		catch (ErrorResponseException e)
		{
			fermerLaConnection();
			throw new Exception(String.format("L'hôte %s est indisponible : %s", nomHote, e.getMessage()));
		}
	}
	
	private void connecterEnModeESMTP_SECURISE(String nomHote, int port)
			throws UnknownHostException, Exception
	{
		mSock = new Socket(nomHote, port);
		
		decorerFluxES();
		
		try
		{
			SMTPUtile.lireLaReponse(in);
		
			SMTPUtile.envoyerUnOrdre(out, OrdresESMTP.ordreHELO(nomHote));
		
			SMTPUtile.lireLaReponse(in);
			
			SMTPUtile.envoyerUnOrdre(out, OrdresESMTP.ordreSTARTTLS);
			
			try
			{
				SMTPUtile.lireLaReponse(in);
			
				mSock = ((SSLSocketFactory)SSLSocketFactory.getDefault()).
										createSocket(mSock, mSock.getInetAddress().getHostAddress(), mSock.getPort(), true);
				
				decorerFluxES();
			}
			catch (ErrorResponseException e)
			{
				fermerLaConnection();
				
				throw new Exception(String.format("Impossible d'établir une connection sécurisée avec l'hôte %s : %s", nomHote, e.getMessage()));
			}
		}
		catch (ErrorResponseException e)
		{
			fermerLaConnection();
			
			throw new Exception(String.format("L'hôte %s est indisponible : %s", nomHote, e.getMessage()));
		}
	}
	
	private void connecter()
			throws UnknownHostException, Exception
	{
		try
		{
			switch (serveur.getTypeDeSecurite())
			{
				case NON_SECURISE:
					System.out.println("Connection en mode non sécurisé...");
					connecterEnModeNonSecurise(serveur.getNomHote(), serveur.getPort());
				break;
		
				case SMTP_VIA_SSL:
					System.out.println("Connection en mode SMTP via SSL...");
					connecterEnModeSMTP_VIA_SSL(serveur.getNomHote(), serveur.getPort());
				break;
				
				case ESMTP_SECURISE:
					System.out.println("Connection ESMTP sécurisé...");
					connecterEnModeESMTP_SECURISE(serveur.getNomHote(), serveur.getPort());
				break;
			}
		
			SMTPUtile.envoyerUnOrdre(out, OrdresESMTP.ordreHELO(serveur.getNomHote()));
			SMTPUtile.lireLaReponse(in);
		}
		catch (ErrorResponseException e)
		{
			fermerLaConnection();
			throw new ConnectionErrorException(String.format("Le serveur est indisponible : %s", e.getMessage()));
		}
		catch (UnknownHostException e)
		{
			throw new ConnectionErrorException(String.format("Impossible de trouver l'hôte \"%s\".", serveur.getNomHote()));
		}
		catch (IOException e)
		{
			throw new ConnectionErrorException("Une erreur réseaux s'est produite.");
		}
		catch (Exception e)
		{
			if (mSock != null)
				fermerLaConnection();
			throw new ConnectionErrorException("Une erreur imprévue s'est produite.");
		}
	}	
	
	private void fermerLaConnection()
			throws Exception
	{	
		try
		{
				SMTPUtile.envoyerUnOrdre(out, OrdresESMTP.ordreQUIT);
				SMTPUtile.lireLaReponse(in);
		}
		catch (IOException e)
		{}
		finally
		{
			try
			{
				mSock.close();
			}
			catch (IOException e){}
		}
	}
	
	private void rechercherLesExtensionsSMTP(List<String> reponses)
			throws ProtocolErrorException, Exception
	{
		for (String reponse : reponses)
		{
			String message = SMTPUtile.message(reponse);
			
			if (message.startsWith("AUTH"))
			{
				verifierTypesAuthentificationSupportes(message.substring(5));
			}
			else if (message.startsWith("8BITMIME"))
			{
				serveur.setSupport8BitMIME(true);
			}
			else if (message.startsWith("SIZE"))
			{
				serveur.setSupportExtensionSize(true);
			}
		}
	}
	
	private void verifierTypesAuthentificationSupportes(String authTypes)
	{
		for (String s : authTypes.split("\\s+"))
		{
			serveur.ajouterTypeAuthentification(s);
		}
	}
	
	private void authentifier(JFrame owner)
			throws SMTPException, Exception
	{
		if (serveur.getMetodeAuthentification() != null)
		{
			try
			{
				serveur.getMetodeAuthentification().authentifier(in, out, owner);
			}
			catch (AuthentificationFailureException e)
			{
				throw new SMTPException(e.getMessage());
			}
		}
	}
	
	private void demanderEnvoiCourrier(String expediteur, long tailleCourrier)
			throws SMTPException,Exception
	{
		if (serveur.supporteExtensionSize())
			SMTPUtile.envoyerUnOrdre(out, OrdresESMTP.ordreMAIL(expediteur, tailleCourrier));
		else
			SMTPUtile.envoyerUnOrdre(out, OrdresESMTP.ordreMAIL(expediteur));
		
		try
		{
			SMTPUtile.lireLaReponse(in);
		}
		catch (ErrorResponseException e)
		{	
			if (e.getErrorCode() == 452)
				throw new SMTPException("Le serveur ne dispose pas de l'espace suffisant pour recevoir le courier.");
			else if (e.getErrorCode() == 552)
				throw new SMTPException("Le courier dépasse la taille maximum autorisée.");
			else
				throw new SMTPException(String.format("Le serveur a revoyé une erreur : %s", e.getMessage()));
		}
	}

	private void evoyerDestinataires(ArrayList<String> destinataires)
			throws SMTPException, Exception
	{
		for (String destinataire : destinataires)
		{
			SMTPUtile.envoyerUnOrdre(out, OrdresESMTP.ordreRCPT(destinataire));
			
			try
			{
				SMTPUtile.lireLaReponse(in);
			}
			catch (ErrorResponseException e)
			{
				throw new SMTPException(String.format("Le serveur à refusé le destinataire <%s> : %s", destinataire, e.getMessage()));
			}
		}
	}
	
	private void envoyerDonnees(Courrier courrier, long tailleCourrier, String charsetTransmission, OperationArrierePlan representation)
			throws SMTPException, Exception
	{
		SMTPUtile.envoyerUnOrdre(out, OrdresESMTP.ordreDATA);
		
		try
		{
			SMTPUtile.lireLaReponse(in);	
		}
		catch (ErrorResponseException e)
		{
			throw new SMTPException(String.format("Le serveur ne peut pas recevoir le courier : %s", e.getMessage()));
		}
			
		out = new BufferedWriterCompteur(new OutputStreamWriter(mSock.getOutputStream(), charsetTransmission),
											representation, 5, 98, tailleCourrier);
		
		System.out.println("envoi du message...");
		
		courrier.ecrire(out);
		
		out.flush();
		
		out = new BufferedWriter(new OutputStreamWriter(mSock.getOutputStream(), "US-ASCII"));
		
		SMTPUtile.envoyerUnOrdre(out, OrdresESMTP.ordreFINCOURIER);
	}
	
	private void attendreConfirmationReception()
			throws SMTPException, Exception
	{	
		try
		{
			SMTPUtile.lireLaReponse(in);
		}
		catch (ErrorResponseException e)
		{
			throw new SMTPException(String.format("Erreur lors de l'envoi du courier : %s", e.getMessage()));
		}
	}
	
	private void envoyerCourier(Courrier courrier, String charsetTransmission, JFrame owner, OperationArrierePlan representation)
			throws Exception
	{
		long tailleCourrier;
		
		try
		{
			representation.setEtape("Calcul de la taille du courrier");
			representation.setProgression(0);
			
			tailleCourrier = courrier.getTaille();
			
			representation.setEtape("Connection au serveur");
			representation.setProgression(1);
			
			connecter();
			
			try
			{	
				representation.setEtape("Authentification");
				representation.setProgression(2);
				
				authentifier(owner);
				
				representation.setEtape("Demande d'envoi");
				representation.setProgression(3);
				
				demanderEnvoiCourrier(courrier.getExpediteur(), tailleCourrier);
				
				representation.setEtape("Envoi des destinataires");
				representation.setProgression(4);
				
				evoyerDestinataires(courrier.getTousDestinataires());
				
				representation.setEtape("Envoi en cours");
				representation.setProgression(5);
				
				envoyerDonnees(courrier, tailleCourrier, charsetTransmission, representation);
				
				representation.setEtape("Envoyé, attente confirmation");
				representation.setProgression(99);
				
				attendreConfirmationReception();
				
				representation.setEtape("Fermeture de la connection");
				representation.setProgression(100);
			}
			catch (SMTPException e)
			{
				throw new Exception(e.getMessage());
			}
			finally
			{
				fermerLaConnection();
			}
		}
		catch (ConnectionErrorException e)
		{
			throw new Exception(String.format("Impossible de se connecter à %s :\n%s", serveur.getNomHote(), e.getLocalizedMessage()));
		}
		catch (Exception e)
		{
			e.printStackTrace(System.err);
			throw new Exception("Une erreur imprévue s'est produite.");
		}
	}
	
	public ControleurSMTP(CompteUtilisateur serveur)
	{
		this.serveur = serveur;
	}
	
	public void consulterLesExtensionsSupportees(OperationArrierePlan representation)
			throws Exception
	{	
		try
		{
			representation.setEtape("Connection..");
			connecter();
			
			representation.setProgression(50);
			representation.setEtape("Interrogation..");
			
			SMTPUtile.envoyerUnOrdre(out, OrdresESMTP.ordreEHLO(serveur.getNomHote()));
			
			try
			{
				SMTPUtile.lireLaReponse(in);
				
				representation.setProgression(80);
				representation.setEtape("Décodage de la reponse..");
				
				rechercherLesExtensionsSMTP(SMTPUtile.lireUneReponseMultiligne(in));
			}
			catch (ErrorResponseException e)
			{
				if (e.getErrorCode() == 421)
				{
					throw new Exception("Le serveur est indisponible");
				}
				else
				{
					throw new Exception("Impossible de détecter les extensions du serveur.");
				}
			}
			finally
			{
				fermerLaConnection();
			}
		}
		catch (ConnectionErrorException e)
		{
			throw new Exception(String.format("Impossible de se connecter à %s :\n%s", serveur.getNomHote(), e.getLocalizedMessage()));
		}
		catch (Exception e)
		{
			e.printStackTrace(System.err);
			throw new Exception("Une erreur imprévue s'est produite.");
		}
	}
	
	public void encapsulerEtEnvoyerCourierSimple(	
			String[] destinataires, 
			String[] destinatairesCopiesCarbone,
			String[] destinatairesCopiesCarboneInvisibles, 
			String sujet,
			String texte,
			DefaultListModel<File> piecesJointes,
			JFrame owner,
			OperationArrierePlan representation)
		throws Exception
	{
		Courrier courrier;
		MessageMIMESimple message;
		String charsetTransmission;
		
		representation.setEtape("Encapsulation du courrier");
		
		if (serveur.supportExtension8BitMIME())
		{
			charsetTransmission = "UTF-8";
			message = new MessageMIMESimple(texte, ContentEncoding.MIME8Bit);
		}
		else
		{
			charsetTransmission = "US-ASCII";
			message = new MessageMIMESimple(texte, ContentEncoding.Base64);
		}
		
		if (piecesJointes.size() > 0)
		{
			ContenuMIMEMultiParties enclosing = new ContenuMIMEMultiParties("multipart/mixed");
			
			enclosing.ajouterSousPartie(message);
			
			for (int i=0; i<piecesJointes.size(); i++)
			{
				enclosing.ajouterSousPartie(new PieceJointeMIME(piecesJointes.get(i),
												ContentDisposition.Attachement,
												ContentEncoding.Base64));
			}
			
			courrier = new Courrier(serveur.getAdresseMail(),
									serveur.getNomUtilisateur(),
									destinataires,
									destinatairesCopiesCarbone,
									destinatairesCopiesCarboneInvisibles,
									sujet,
									enclosing);
		}
		else
		{
			courrier = new Courrier(serveur.getAdresseMail(),
									serveur.getNomUtilisateur(),
									destinataires,
									destinatairesCopiesCarbone,
									destinatairesCopiesCarboneInvisibles,
									sujet,
									message);
		}
		
		envoyerCourier(courrier, charsetTransmission, owner, representation);
	}

	/*__________________________________________________________*/
	/**
	 * @param destinataires
	 * @param destinatairesCopiesCarbone
	 * @param destinatairesCopiesCarboneInvisibles
	 * @param sujet
	 * @param codeHTML
	 * @param image
	 * @param piecesJointes
	 * @param editeur
	 * @param operationArrierePlan
	 * @throws Exception 
	 */
	public void encapsulerEtEnvoyerCourierHTML(String[] destinataires,
			String[] destinatairesCopiesCarbone,
			String[] destinatairesCopiesCarboneInvisibles,
			String sujet,
			String codeHTML,
			HashMap<URL, File> images,
			DefaultListModel<File> piecesJointes,
			JFrame owner,
			OperationArrierePlan representation) throws Exception
	{
		Courrier courrier;
		MessageMIMEHTMLEtImages message;
		String charsetTransmission;
		
		representation.setEtape("Encapsulation du courrier");
		
		if (serveur.supportExtension8BitMIME())
		{
			charsetTransmission = "UTF-8";
			message = new MessageMIMEHTMLEtImages(codeHTML, images, ContentEncoding.MIME8Bit, ContentEncoding.Base64);
		}
		else
		{
			charsetTransmission = "US-ASCII";
			message = new MessageMIMEHTMLEtImages(codeHTML, images, ContentEncoding.Base64, ContentEncoding.Base64);
		}
		
		if (piecesJointes.size() > 0)
		{
			ContenuMIMEMultiParties enclosing = new ContenuMIMEMultiParties("multipart/mixed");
			
			enclosing.ajouterSousPartie(message);
			
			for (int i=0; i<piecesJointes.size(); i++)
			{
				enclosing.ajouterSousPartie(new PieceJointeMIME(piecesJointes.get(i),
												ContentDisposition.Attachement,
												ContentEncoding.Base64));
			}
			
			courrier = new Courrier(serveur.getAdresseMail(),
									serveur.getNomUtilisateur(),
									destinataires,
									destinatairesCopiesCarbone,
									destinatairesCopiesCarboneInvisibles,
									sujet,
									enclosing);
		}
		else
		{
			courrier = new Courrier(serveur.getAdresseMail(),
									serveur.getNomUtilisateur(),
									destinataires,
									destinatairesCopiesCarbone,
									destinatairesCopiesCarboneInvisibles,
									sujet,
									message);
		}
		
		envoyerCourier(courrier, charsetTransmission, owner, representation);
		
	}
}

/*__________________________________________________________*/
/*   Fin du fichier ControleurSMTP.java
/*__________________________________________________________*/