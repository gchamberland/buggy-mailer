/*__________________________________________________________*/
/*__________________________________________________________*/
/**
 * Fichier : SMTPUtile.java
 * 
 * créé le : 15 févr. 2014 à 18:26:03
 *
 * Auteur : Grégoire Chamberland
 */
/*__________________________________________________________*/
package controleur;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import modele.smtp.exceptions.ErrorResponseException;
import modele.smtp.exceptions.ProtocolErrorException;

/*__________________________________________________________*/
/**
 */
/*__________________________________________________________*/
public class SMTPUtile
{
	public static String message(String reponse)
	{
		return reponse.substring(4);
	}
	
	private static int codeReponse(String reponse)
		throws NumberFormatException
	{
		Integer code = Integer.parseInt(reponse.substring(0, 3));
		
		return code;
	}
	
	private static boolean estCodeDeSucces(int codeReponse)
	{
		return codeReponse > 199 && codeReponse < 300 || codeReponse == 354;
	}
	
	private static String lire(BufferedReader in) throws Exception
	{
		String reponse = in.readLine();
		
		if (reponse == null)
			throw new IOException("La connection à été fermée.");
		
		//Pendant le développement seulement
		System.out.println(reponse);
		//Pendant le développement seulement
		
		return reponse;
	}
	
	public static void envoyerUnOrdre(BufferedWriter out, String message) throws Exception
	{
		StringBuffer str = new StringBuffer(message);
		
		str.append("\r\n");
		
		out.write(str.toString());
		
		out.flush();
		
		//Pendant le développement seulement
		System.out.println(str.toString());
		//Pendant le développement seulement
	}
	
	public static String lireLaReponse(BufferedReader in) throws Exception
	{	
		try
		{
			String reponse = lire(in);
			
			if (!estCodeDeSucces(codeReponse(reponse)))
				throw new ErrorResponseException(message(reponse), codeReponse(reponse));
			
			return reponse;
		}
		catch (NumberFormatException e)
		{
			throw new ProtocolErrorException("Le serveur n'a pas respecté le protocole.");
		}
	}
	
	public static List<String> lireUneReponseMultiligne(BufferedReader in) throws Exception
	{
		List<String> lignesReponses = new ArrayList<String>();
		ErrorResponseException exception = null;
		
		do
		{
			
			try
			{
				lignesReponses.add(lireLaReponse(in));
			}
			catch (ErrorResponseException e)
			{
				exception = e;
			}
		}
		while (in.ready());
		
		if (exception != null)
			throw exception;
		
		return lignesReponses;
	}
}

/*__________________________________________________________*/
/*   Fin du fichier SMTPUtile.java
/*__________________________________________________________*/