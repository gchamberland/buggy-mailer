/*__________________________________________________________*/
/*__________________________________________________________*/
/**
 * Fichier : Main.java
 * 
 * créé le : 8 févr. 2014 à 14:53:17
 *
 * Auteur : Grégoire Chamberland
 */
/*__________________________________________________________*/
package controleur;

import java.awt.Image;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.net.UnknownHostException;
import java.net.UnknownServiceException;
import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.swing.DefaultListModel;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;
import javax.swing.UIManager;
import javax.swing.UIManager.LookAndFeelInfo;
import modele.smtp.CompteUtilisateur;
import vue.DialogueAjoutServeur;
import vue.DialogueAuthentification;
import vue.FenetrePricipale;

/*__________________________________________________________*/
/**
 */
/*__________________________________________________________*/
public class Controleur
{	
	private static HashMap<String, ImageIcon> icones = new HashMap<String, ImageIcon>();
	
	private static final String[] typeAuthentificationSupportes = new String[] { "PLAIN" };
	
	private DefaultListModel<CompteUtilisateur> listeServeurs;
	
	private FenetrePricipale editeur;
	
	public Controleur()
	{
		listeServeurs = Persistance.chargerListeServeurs(null);
		
		editeur = new FenetrePricipale(listeServeurs, OperationArrierePlan.getListeOperations(), this);
		editeur.setVisible(true);
	}
	
	public static String[] getTypeAuthSupportes()
	{
		return typeAuthentificationSupportes;
	}
	
	public void supprimerServeur(CompteUtilisateur serveur)
	{
		listeServeurs.removeElement(serveur);
		Persistance.enregistrerServeurs(listeServeurs, editeur);
	}
	
	public void modifierServeur(final CompteUtilisateur ancienServeur)
	{
		new OperationArrierePlan("Modification d'un serveur")
		{	
			@Override
			public void run()
			{
				try
				{
					CompteUtilisateur serveur;
					DialogueAjoutServeur dialogConnexion = new DialogueAjoutServeur(editeur, ancienServeur);
					DialogueAuthentification dialogAuth;
					
					setEtape("Demande d'informations");
					setProgression(10);
					
					dialogConnexion.setVisible(true);
					
					serveur = dialogConnexion.getServeur();
					
					if (serveur == null)
					{
						setTerminee();
						return;
					}
					
					try
					{	
						ControleurSMTP smtp = new ControleurSMTP(serveur);
						
						smtp.consulterLesExtensionsSupportees(this);
					}
					catch (Exception e)
					{
						setTerminee();
						afficherErreur(e);
						return;
					}
					
					serveur.setAuthentificationMethod(ancienServeur.getMetodeAuthentification());
					
					dialogAuth = new DialogueAuthentification(editeur, serveur);
					
					setEtape("Demande d'informations");
					setProgression(90);
					
					dialogAuth.setVisible(true);
					
					serveur = dialogAuth.getServeur();
					
					if (serveur == null)
					{
						setTerminee();
						return;
					}
					
					listeServeurs.removeElement(ancienServeur);
					listeServeurs.addElement(serveur);
					
					setEtape("Enregistrement...");
					setProgression(95);
					
					Persistance.enregistrerServeurs(listeServeurs, editeur);
					
					setTerminee();
				}
				catch (Exception e)
				{
					afficherErreur(e);
					setTerminee();
				}
			}
		}.start();
	}
	
	public void ajouterServeur()
	{	
		new OperationArrierePlan("Ajout d'un serveur")
		{	
			@Override
			public void run()
			{
				try
				{
					CompteUtilisateur serveur;
					DialogueAjoutServeur dialogConnexion = new DialogueAjoutServeur(editeur);
					DialogueAuthentification dialogAuth;
					
					setEtape("Demande d'informations");
					setProgression(10);
					
					dialogConnexion.setVisible(true);
					
					serveur = dialogConnexion.getServeur();
					
					if (serveur == null)
					{
						setTerminee();
						return;
					}
					
					try
					{	
						ControleurSMTP smtp = new ControleurSMTP(serveur);
						
						smtp.consulterLesExtensionsSupportees(this);
					}
					catch (Exception e)
					{
						setTerminee();
						afficherErreur(e);
						return;
					}
					
					dialogAuth = new DialogueAuthentification(editeur, serveur);
					
					setEtape("Demande d'informations");
					setProgression(90);
					
					dialogAuth.setVisible(true);
					
					serveur = dialogAuth.getServeur();
					
					if (serveur == null)
					{
						setTerminee();
						return;
					}
					
					listeServeurs.addElement(serveur);
					
					setEtape("Enregistrement...");
					setProgression(95);
					
					Persistance.enregistrerServeurs(listeServeurs, editeur);
					
					setTerminee();
				}
				catch (Exception e)
				{
					afficherErreur(e);
					setTerminee();
				}
			}
		}.start();
	}
	
	public void envoyerCourierSimple(final CompteUtilisateur serveur, 
			final String[] destinataires, 
			final String[] destinatairesCopiesCarbone,
			final String[] destinatairesCopiesCarboneInvisibles,
			final String sujet,
			final String texte,
			final DefaultListModel<File> piecesJointes)
	{
		new OperationArrierePlan("Envoi d'un courier")
		{
			@Override
			public void run()
			{	
				try
				{
					ControleurSMTP controleur = new ControleurSMTP(serveur);
					
					controleur.encapsulerEtEnvoyerCourierSimple(destinataires,
							destinatairesCopiesCarbone,
							destinatairesCopiesCarboneInvisibles,
							sujet,
							texte,
							piecesJointes,
							editeur,
							this);
					
					setTerminee();
					
				} catch (Exception e)
				{
					afficherErreur(e);
					setTerminee();
				}
			}
		}.start();
	}
	
	public void envoyerCourierHTML(final CompteUtilisateur serveur, 
			final String[] destinataires, 
			final String[] destinatairesCopiesCarbone,
			final String[] destinatairesCopiesCarboneInvisibles,
			final String sujet,
			final String codeHTML,
			final DefaultListModel<File> piecesJointes)
	{
		new OperationArrierePlan("Envoi d'un courier")
		{
			@Override
			public void run()
			{	
				try
				{
					ControleurSMTP controleur = new ControleurSMTP(serveur);
					HashMap<URL, File> images = null;
					
					setEtape("Obtention des images...");
					images = getImages(codeHTML);
					
					controleur.encapsulerEtEnvoyerCourierHTML(destinataires, 
							destinatairesCopiesCarbone, 
							destinatairesCopiesCarboneInvisibles, 
							sujet, 
							codeHTML,
							images,
							piecesJointes, 
							editeur, 
							this);
					
					setEtape("Suppression des images téléchargées..");
					
					for (URL url : images.keySet())
					{
						if (!url.getProtocol().toLowerCase().equals("file"))
							System.out.println(images.get(url).delete());
					}
					
					setTerminee();
					
				} catch (Exception e)
				{
					afficherErreur(e);
					setTerminee();
				}
			}
		}.start();
	}
	
	private HashMap<URL, File> getImages(String codeHTML) throws Exception
	{
		HashMap<URL, File> images = new HashMap<URL, File>();
		
		Pattern pattern = Pattern.compile("<img .*src=\"([^\"]*)\"", Pattern.MULTILINE | Pattern.CASE_INSENSITIVE);
		
		Matcher match = pattern.matcher(codeHTML);
		
		while (match.find())
		{
				try
				{
					URL imageURL = new URL(match.group(1));
					
					if (images.containsKey(imageURL))
						continue;
					
					if (imageURL.getProtocol().toLowerCase().equals("file"))
					{
						images.put(imageURL, new File(imageURL.toURI()));
					}
					else
					{
						images.put(imageURL, saveFile(imageURL));
					}
					
			} catch (MalformedURLException e)
			{
				throw new Exception(String.format("L'URL \"%s\" n'est pas valide.", match.group(1)));
			}
		}
		
		return images;
	}
	
	private static File saveFile(URL url) throws Exception
	{	
		File f = null;
		URLConnection connection = null;
		String contentType = null;
		
		try
		{
			f = File.createTempFile("buggyTempFile", "");
			f.deleteOnExit();
		}
		catch (Exception e)
		{
			throw new Exception("Impossible de creer un fichier temporaire pour stocker une image externe.");
		}
		
		try
		{
			connection = url.openConnection();
			contentType = connection.getContentType();
			
			if (contentType != null)
			{
				if (contentType.toLowerCase().startsWith("image"))
				{
					telechargerFichier(f, url, connection);
					return f;
				}
				else
				{
					throw new Exception(String.format("\"%s\" : Le fichier n'éxiste pas ou n'est pas une image.", url));
				}
			}
			else
			{
				telechargerFichier(f, url, connection);
				return f;
			}
			
		}
		catch (IOException e)
		{
			throw new Exception(String.format("Une erreur réseau s'est produite pendant le téléchargement de l'image à l'adresse \"%s\".", url));
		}
	}
		
	private static void telechargerFichier(File f, URL url, URLConnection connection) throws Exception
	{
		BufferedInputStream is = null;
		BufferedOutputStream os = null;
		
		try
		{
			is = new BufferedInputStream(connection.getInputStream());
			os = new BufferedOutputStream(new FileOutputStream(f));
			
			byte[] b = new byte[2048];
			int length;
	 
			while ((length = is.read(b)) != -1) {
				os.write(b, 0, length);
			}
	 
			is.close();
			os.close();
		}
		catch (UnknownServiceException e)
		{
			throw new Exception(String.format("Impossible de télécharger l'image depuis l'URL \"%s\".", url));
		}
		catch (UnknownHostException e)
		{
			throw new Exception(String.format("Impossible de télécharger l'image à l'adresse \"%s\" : hôte introuvable.", url));
		}
		catch (IOException e)
		{
			throw new Exception(String.format("Une erreur réseaux s'est produite pendant le téléchargement de l'image à l'URL \"%s\".", url));
		}
		catch (Exception e)
		{
			throw new Exception(String.format("Une erreur inatendue s'est produite pendant le téléchargement de l'image à l'URL \"%s\".", url));
		}
		finally
		{
			try
			{
				if (is != null)
					is.close();
				
				if (os != null)
					os.close();
			}
			catch (IOException e)
			{}
		}
	}
	
	private void afficherErreur(Exception e)
	{
		e.printStackTrace(System.err);
		JOptionPane.showMessageDialog(editeur, 
				e.getLocalizedMessage(), 
				"Une erreur s'est produite", JOptionPane.ERROR_MESSAGE);
	}
	
	public static String getHumanReadable(float nBytes)
	{	
		if (nBytes < 1024)
		{
			return String.format("%.2f o", nBytes);
		}
		else if (nBytes < 1048576)
		{
			return String.format("%.2f Ko", nBytes / 1024);
		}
		else if (nBytes < 1073741824)
		{
			return String.format("%.2f Mo", nBytes / 1048576);
		}
		else
		{
			return String .format("%.2f Go", nBytes / 1073741824);
		}
	}

	public static ImageIcon getImage(String name, int width, int height)
	{
		if (icones.containsKey(name))
		{
			if (icones.get(name).getIconWidth() == width && icones.get(name).getIconHeight() == height)
				return icones.get(name);
		}
		
		ImageIcon image = new ImageIcon(
				new ImageIcon(
						Controleur.class.getResource(String.format("/%s", name)))
							.getImage().getScaledInstance(width, height, Image.SCALE_SMOOTH));
		
		icones.put(name, image);
		
		return image;
	}

	public static void main(String[] args)
	{
		try
		{
			for (LookAndFeelInfo info : UIManager.getInstalledLookAndFeels()) {
		        if ("Nimbus".equals(info.getName())) {
		            UIManager.setLookAndFeel(info.getClassName());
		            break;
		        }
		    }
		} catch (Exception e)
		{}
		
		new Controleur();

	}
}

/*__________________________________________________________*/
/*   Fin du fichier Main.java
/*__________________________________________________________*/