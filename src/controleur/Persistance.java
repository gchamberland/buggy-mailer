/*__________________________________________________________*/
/*__________________________________________________________*/
/**
 * Fichier : Persistance.java
 * 
 * créé le : 15 févr. 2014 à 16:26:46
 *
 * Auteur : Grégoire Chamberland
 */
/*__________________________________________________________*/
package controleur;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStreamWriter;

import javax.swing.DefaultListModel;
import javax.swing.JFrame;
import javax.swing.JOptionPane;

import modele.smtp.CompteUtilisateur;
import modele.smtp.CompteUtilisateur.TypeDeSecurite;

/*__________________________________________________________*/
/**
 */
/*__________________________________________________________*/
class Persistance
{
	private static final String CHEMIN_FICHIER_SERVEURS = String.format("%s/.buggy", System.getProperty("user.home"));
	
	/*__________________________________________________________*/
	/**
	 * @return
	 */
	public static DefaultListModel<CompteUtilisateur> chargerListeServeurs(JFrame vue)
	{
		File f = new File(CHEMIN_FICHIER_SERVEURS);
		ObjectInputStream objstr = null;
		
		if (f.exists() && f.canRead())
		{
			try
			{
				objstr = new ObjectInputStream(new FileInputStream(f));
				
				DefaultListModel<CompteUtilisateur> listeServeurs = (DefaultListModel<CompteUtilisateur>) objstr.readObject();
				
				return listeServeurs;
			}
			catch (FileNotFoundException e)
			{}
			catch (Exception e)
			{
				JOptionPane.showMessageDialog(vue, 
						"Le fichier de configuration est corrompu.\n Il va être supprimé.", 
						"Impossible de charger la liste des serveurs", JOptionPane.ERROR_MESSAGE);
				
				f.delete();
			}
			finally
			{
				try
				{
					if (objstr != null)
						objstr.close();
				} catch (Exception e2){}
			}
		}
		
		return new DefaultListModel<CompteUtilisateur>();
	}

	public static void enregistrerServeurs(DefaultListModel<CompteUtilisateur> listeServeurs, JFrame vue)
	{	
		File f = new File(CHEMIN_FICHIER_SERVEURS);
		ObjectOutputStream objstr = null;
		
		try
		{
			f.createNewFile();
			f.setReadable(false, false);
			f.setReadable(true);
			
			objstr = new ObjectOutputStream(new FileOutputStream(f));
			
			objstr.writeObject(listeServeurs);
		}
		catch (Exception e)
		{
			JOptionPane.showMessageDialog(vue, 
					"Une erreur s'est produite pendant l'écriture du fichier de configuration.", 
					"Impossible de sauvegarder la liste des serveurs", JOptionPane.ERROR_MESSAGE);
		}
		finally
		{
			try
			{
				if (objstr != null)
					objstr.close();
			} catch (IOException e2){}
		}
	}
}

/*__________________________________________________________*/
/*   Fin du fichier Persistance.java
/*__________________________________________________________*/