/*__________________________________________________________*/
/*__________________________________________________________*/
/**
 * Fichier : OperationArrierePlan.java
 * 
 * créé le : 14 févr. 2014 à 21:17:46
 *
 * Auteur : Grégoire Chamberland
 */
/*__________________________________________________________*/
package controleur;

import java.util.ArrayList;

import javax.swing.BoundedRangeModel;
import javax.swing.DefaultBoundedRangeModel;
import javax.swing.DefaultListModel;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import modele.ListModeleAttentive;

/*__________________________________________________________*/
/**
 */
/*__________________________________________________________*/
public abstract class OperationArrierePlan extends Thread
{
	private static ListModeleAttentive listeOperations = new ListModeleAttentive();
	
	private String description;
	private String etape;
	private BoundedRangeModel progression;
	
	private ArrayList<ChangeListener> listeners;
	
	/*__________________________________________________________*/
	/**
	 * @param description
	 * @param etape
	 * @param progression
	 */
	public OperationArrierePlan(String description)
	{
		listeners = new ArrayList<ChangeListener>();
		
		setDescription(description);
		setEtape("");
		setProgressionModel(new DefaultBoundedRangeModel(0, 0, 0, 100));
		
		listeOperations.addElement(this);
	}
	
	public OperationArrierePlan()
	{
		this("");
	}
	
	public void addChangeListener(ChangeListener listener)
	{
		listeners.add(listener);
	}
	
	public void removeChangeListener(ChangeListener listener)
	{
		listeners.remove(listener);
	}
	
	public void setDescription(String description)
	{
		this.description = description;
		onChanged();
	}
	
	public void setEtape(String etape)
	{
		this.etape = etape;
		onChanged();
	}
	
	public void setProgressionModel(BoundedRangeModel modele)
	{
		this.progression = modele;
		onChanged();
	}
	
	public void setProgression(int progression)
	{
		this.progression.setValue(progression);
		onChanged();
	}
	
	protected void setTerminee()
	{
		setEtape("Terminé.");
		setProgression(100);
		
		listeOperations.removeElement(this);
	}
	
	public BoundedRangeModel getProgressionModele()
	{
		return progression;
	}
	
	public static ListModeleAttentive getListeOperations()
	{
		return listeOperations;
	}


	/*__________________________________________________________*/
	/** Permet d'obtenir la valeur du champ description
	 * @return La valeur du champ description.
	 */
	public String getDescription()
	{
		return description;
	}

	/*__________________________________________________________*/
	/** Permet d'obtenir la valeur du champ etape
	 * @return La valeur du champ etape.
	 */
	public String getEtape()
	{
		return etape;
	}

	/*__________________________________________________________*/
	/**
	 */
	private void onChanged()
	{
		for (ChangeListener listener : listeners)
			listener.stateChanged(new ChangeEvent(this));
	}
}

/*__________________________________________________________*/
/*   Fin du fichier OperationArrierePlan.java
/*__________________________________________________________*/