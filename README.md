# README #

BuggyMailer is a cross-platform SMTP client writted in Java.
It support SMTP over SSL (port 465) and Secured ESMTP (submission, port 587).

As it's a student project, it does nothing more, or better than another SMTP client.
But it is perfectly functional, and I haven't found any bug yet.

Remember it is an SMTP client and it just can send mail !

##Summary##

1. How to get BuggyMailer
2. How to get the sources

##How to get BuggyMailer##

Java (>= version 6) has to be installed on your system.

###Windows###

Download the .msi package in build/windows/BuggyMailer-setup/bin.
Install it. Good game !

###Others###

Download the "BuggyMailer.jar" file into the build directory of this repository and launch it.

###Important information###
You have to know your mail accounts's passwords are stored in a file named ".Buggy" in your home directory.

##How to get the sources##

To download the sources :

git clone https://gchamberland@bitbucket.org/gchamberland/buggy-mailer.git

This repository contains a whole Eclipse project.