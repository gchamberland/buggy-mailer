This directory contains tools necessary to create a msi package for BuggyMailer.
It include :

	- A Launch4j configuration file (To encapsulate the BuggyMailer.jar file in a Windows executable).
	- A Visual Studio (2013) solution containing a Windows Installer Xml project (To create a Windows Installer package).
	
The final .msi file is in "BuggyMailer-setup\bin"